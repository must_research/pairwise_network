import os
import pytest
from pairwise_network import (env, build, config)


@pytest.mark.parametrize('cfg_filename',
    (
            'test-symh.yaml',
            # 'test-epsv2.yaml',
    )
)
def test_run_train(cfg_filename):
    env.set_log_level('DEBUG')
    cfg_path = os.path.join(env.test_config_dir, cfg_filename)
    cfg = config.cfg_from_yaml(cfg_path)
    build.run_train(cfg=cfg, use_wandb=False)
    assert True


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    # cfg_filename = 'test-epsv2.yaml'
    cfg_filename = 'test-symh.yaml'
    test_run_train(cfg_filename)
# -----------------------------------------------------------------------------
