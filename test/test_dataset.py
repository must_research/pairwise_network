import os
import logging
import pytest
from pairwise_network import (env, config, dataset)


@pytest.mark.skip
@pytest.mark.parametrize('cfg_filename',
    (
            'test-symh.yaml',
            'test-epsv2.yaml',
            'test-epsv3.yaml',
    )
)
def test_load_symh_dataset(cfg_filename):
    env.set_log_level('INFO')
    config_path = os.path.join(env.test_config_dir, cfg_filename)
    cfg = config.cfg_from_yaml(config_path)
    datasets = dataset.create_dataset_from_cfg(cfg)
    dataloaders = dataset.create_dataloaders_from_cfg(cfg)
    assert True

@pytest.mark.parametrize(('cfg_filename', 'partition'),
    (
            ('test-symh.yaml', 'train'),
            ('test-art-snd.yaml', 'eval'),
            ('test-art-snd.yaml', 'test'),
    )
)
def test_dataset_to_partition(cfg_filename, partition):
    env.set_log_level('INFO')
    config_path = os.path.join(env.test_config_dir, cfg_filename)
    cfg = config.cfg_from_yaml(config_path)
    datasets = dataset.create_dataset_from_cfg(cfg)
    dataset.datasets_to_partition(datasets, partition)
    assert True
