import logging
import pytest

from pairwise_network import env, dataset, net, wandb_api, build


@pytest.mark.parametrize('run_id', ('3ppe7nga',))
def test_download_evaluate_model(run_id):
    env.set_log_level()
    run = wandb_api.runs.run_id_to_run(run_id)
    cfg = wandb_api.load.run_to_cfg(run)

    env.set_seeds(cfg['seed'])

    dataloaders = dataset.create_dataset_from_cfg(cfg)

    # Model
    model, pairwise_structure, mlp_mask = \
        net.create_pairwise_model_from_cfg(cfg)

    _, df = build.evaluate_model(model, dataloaders, device=cfg['device'])
    logging.info('Evaluation Results:\n' + df.to_markdown())


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    test_download_evaluate_model('3ppe7nga')
# -----------------------------------------------------------------------------
