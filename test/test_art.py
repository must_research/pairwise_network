import os

import pytest
import matplotlib.pyplot as plt

from pairwise_network.art import art_core, art_generate
from pairwise_network import env, config


@pytest.mark.skip
def test_generate_snd(plot=False):
    df = art_core.create_snd(
        sympy_func='x1 * x2',
        input_tags=['x1', 'x2', 'x3'],
        output_tag='y',
        n_samples=200,
        output_noise=(0, 0.1),
    )
    if plot:
        df.plot(subplots=True)
        plt.show()
    assert True


@pytest.mark.skip
def test_split(plot=True):
    output_tag = 'y'
    df = art_core.create_snd(
        sympy_func='x1 * x2',
        input_tags=['x1', 'x2', 'x3'],
        output_tag=output_tag,
        n_samples=200,
        output_noise=(0, 0.1)
    )
    df_datasets = art_core.split_df(df, 'shuffle', 15, 15)
    if plot:
        for df in df_datasets:
            df[output_tag].plot(style='.')
        plt.legend(('train', 'valid', 'eval'))
        plt.show()
        plt.close()
    assert True


@pytest.mark.skip
@pytest.mark.parametrize(('valid_perc', 'reload'), ((0, False), (0, False),
(20, False), (20, True),))
def test_snd(valid_perc, reload):
    env.set_log_level('INFO')
    art_generate.snd(output_function='x1*x2', input_tags=['x1', 'x2', 'x3'],
        output_tag='y', n_samples=200, output_noise=None,
        split_method='shuffle', valid_perc=valid_perc, eval_perc=0, seed=1,
        reload=reload)
    assert True


@pytest.mark.parametrize('cfg_name', ('test-art-snd.yaml',))
def test_art(cfg_name):
    env.set_log_level()
    cfg_path = os.path.join(env.test_config_dir, cfg_name)
    cfg = config.cfg_from_yaml(cfg_path)
    df_datasets = art_generate.create_art_dataset(cfg)
    assert True
    # return df_datasets


