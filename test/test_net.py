import pytest
import torch

from pairwise_network import net


def test_state_dict():
    model, _, _ = net.create_pairwise_model(
        inputs=['x1', 'x2', 'x3'],
        hidden=[10, 10],
    )
    model_path = 'output/model.pt'
    torch.save(model.state_dict(), model_path)
    torch.load(model_path)