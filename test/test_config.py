import os
import pytest
from pairwise_network import (env, config)


@pytest.mark.parametrize('cfg_filename',
    (
            'test-symh.yaml',
            'test-epsv2.yaml',
            'test-epsv3.yaml',
    )
)
def test_load_config(cfg_filename):
    config_path = os.path.join(env.test_config_dir, cfg_filename)
    cfg = config.cfg_from_yaml(config_path)
    assert True
