__author__ = 'jpbeukes27@gmail.com'

import numpy as np


def correlation(y_pred, y_true):
    """Pearson correlation between two arrays."""
    return np.corrcoef(y_pred, y_true, rowvar=False)[0, 1]