__author__ = 'jpbeukes27@gmail.com'

from sys import exit
import logging
import re
import sympy


# TODO: Add functions to convert labels to latex

def sympy_func_to_latex(func):
    """Converts a sympy-compatible function to latex.

    Args:
        func (str): sympy-compatible equation.

    Returns:
        str: Latex function.
    """
    eq = sympy.sympify(func)
    result = sympy.latex(eq)
    return result


def param_to_latex(param):
    match = re.match(r'(.)(\d)(\*)?', param)
    if match is None:
        return param
    else:
        x = match.group(1)
        n = match.group(2)
        s = match.group(3)
        if s is None:
            s = ''
        return f'${x}_{n}{s}$'


def pair_to_latex(tag):
    pair = split_pairs(tag)
    result_list = [param_to_latex(x) for x in pair]
    result = ', '.join(result_list)
    return result


def split_pairs(tag):
    shifted_tags = re.findall(r'(\w+\[t-\d+])', tag)
    result = shifted_tags
    tag_shift_removed = re.sub(r'-?\w+\[.+]', '', tag)
    if not tag_shift_removed == '':
        remaining_tags = re.split('[-]+', tag_shift_removed)
        result.extend(remaining_tags)
    return result


def tags_replace_substring(tags, substring, new_substring=''):
    return [tag.replace(substring, new_substring) for tag in tags]
