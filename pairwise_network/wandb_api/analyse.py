import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import logging

from pairwise_network import env, dataset, wandb_api, labels
from pairwise_network.analyse.activation import run_sn_act_dist, \
    plot_act_dist, load_sn_activation_df


def run_to_sn_act(run, partition='train', weighted=True, reload_act=False,
        reload_data=False, reload_model=False):
    """Wraps analyse.activation.run_sn_act_dist: Calculate the summary node \
    (sn) activation distribution and return as a Series.

    Args:
        run (Run):              wandb run
        partition (str):        partition of dataset to use (train, valid, eval)
        weighted (bool):        if True, sn activation is scaled by sn weights.
        reload_act (bool):      if True, activation values are recalculated.
        reload_data (bool):     if True, the dataset is re-generated.
        reload_model (bool):    if True, the all model parameter files are
            re-downloaded from wandb.
        add_star (bool):        add an asterisk to subnet tags where only one
            input parameter remains after pruning.

    Returns:
        DataFrame: Summary node activations.

    """
    # Setup
    cfg = wandb_api.run_to_cfg(run)
    env.set_seeds(cfg['seed'])
    file_path = env.get_run_sn_act_path(run.id, partition)

    # Dataset
    datasets, input_tags = dataset.create_dataset_from_cfg(cfg, reload_data)
    x = dataset.datasets_to_partition(datasets, partition).data

    # Model
    model, pairwise_structure, _ = wandb_api.run_to_model(run, reload_model)

    # Activation Values
    result = load_sn_activation_df(x, model, pairwise_structure, file_path,
        reload_activations=reload_act, weighted=weighted)

    return result


def run_to_sn_act_dist(run, partition='train', rename_subnets=True,
        weighted=True, reload_act=False, reload_data=False, reload_model=False,
        add_star=True, return_renamed_tags=False, act_mask=None):
    """Wraps analyse.activation.run_sn_act_dist: Calculate the summary node \
    (sn) activation distribution and return as a Series.

    Args:
        add_star:
        run (Run):              wandb run
        partition (str):        partition of dataset to use (train, valid, eval)
        rename_subnets (bool):  if True subnets are renamed to reflect the
            remaining input parameters, if some of the input parameters are
            pruned away in layer 2 of the network.
        weighted (bool):        if True, sn activation is scaled by sn weights.
        reload_act (bool):      if True, activation values are recalculated.
        reload_data (bool):     if True, the dataset is re-generated.
        reload_model (bool):    if True, the all model parameter files are
            re-downloaded from wandb.
        add_star (bool):        add an asterisk to subnet tags where only one
            input parameter remains after pruning.
        return_renamed_tags (bool): if True, returns a list of the renamed
                                subnet tags. (Only available if `rename_subnets`
                                is True.)

    Returns:
        Series: Summary node activation distribution.

    """
    # Setup
    cfg = wandb_api.run_to_cfg(run)
    env.set_seeds(cfg['seed'])
    file_path = env.get_run_sn_act_path(run.id, partition)

    # Dataset
    datasets, input_tags = dataset.create_dataset_from_cfg(cfg, reload_data)
    x = dataset.datasets_to_partition(datasets, partition).data

    # Model
    model, pairwise_structure, _ = wandb_api.run_to_model(run, reload_model)

    return run_sn_act_dist(x, model, pairwise_structure, input_tags,
        file_path, reload_act, weighted, rename_subnets, add_star,
        return_renamed_tags, act_mask)


def plot_runs_sn_act_dist(runs, partition='valid', rename_subnets=True,
        weighted=True, reload_act=False, reload_data=False, reload_model=False,
        add_star=True, ax=None, legend='none', cmap=None,
        add_xtick_metrics=True, use_symh_tags=False, drop_tags=None, ncols=1,
        metric_format='%.2f', bbox_to_anchor=(1, 1)
):
    """Plot the summary node (sn) activation distributions of multiple runs
    on a single chart.

    Args:
        runs (list):            wandb runs.
        partition (str):        partition of dataset to use (train, valid, eval)
        rename_subnets (bool):  if True subnets are renamed to reflect the
                                remaining input parameters, if some of the
                                input parameters are pruned away in layer 2
                                of the network.
        weighted (bool):        if True, sn activation is scaled by sn weights.
        reload_act (bool):      if True, activation values are recalculated.
        reload_data (bool):     if True, the dataset is re-generated.
        reload_model (bool):    if True, the all model parameter files are
                                re-downloaded from wandb.
        add_star (bool):        add an asterisk to subnet tags where only one
                                input parameter remains after pruning.
        hide_zero_act (bool):   if True, subnets without any sn activation
                                will not be shown.
        legend (str):           options: bottom, right, none

    """
    act_dist_list = list()
    xtick_labels = list()
    if add_xtick_metrics:
        p = partition.capitalize()
        xtick_metrics = ('Seed', f'{p} MSE', f'{p} Corr')
        # xtick_metrics = ('Seed', f'{p} MSE', f'{p} Corr', 'Sparsity')
        xtick_labels.append('\n'.join(xtick_metrics))
    for i, run in enumerate(runs):
        # Calculate activation distribution
        act_dist = run_to_sn_act_dist(run, partition,
            rename_subnets, weighted, reload_act, reload_data, reload_model,
            add_star)  # TODO: check bar colors
        # act_dist = act_dist.loc[act_dist != 0]
        act_dist_list.append(act_dist)

        # Ticks
        cfg = wandb_api.run_to_cfg(run)
        xtick_labels.append('\n'.join([
            str(cfg['seed']),
            # metric_format % run.summary[f'es_{partition}_loss'],
            # metric_format % run.summary[f'es_{partition}_corr'],
            # metric_format % run.summary['es_global_sparsity'],
        ]))
    tags = act_dist_list[0].index.to_list()

    if drop_tags is not None:
        from matplotlib.colors import ListedColormap
        colors = cmap.colors
        new_tags = []
        new_colors = []
        for i, tag in enumerate(tags):
            if tag in drop_tags:
                for act_dist in act_dist_list:
                    act_dist.drop(tag, inplace=True)
            else:
                new_colors.append(colors[i])
                new_tags.append(tag)
        tags = new_tags
        colors = new_colors
        cmap = ListedColormap(colors)
    if use_symh_tags:
        from symh_dataset import labels as symh_labels
        tags = [symh_labels.pair_to_latex(tag, add_star=True) for tag in tags]
    else:
        tags = [labels.pair_to_latex(tag) for tag in tags]

    plot_act_dist(act_dist_list, ax, cmap, width=0.8)

    if ax is None:
        ax = plt.gca()

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)

    ax.tick_params(length=0)

    xticks = np.arange(len(runs))
    if add_xtick_metrics:
        xticks = list(xticks)
        xticks.insert(0, -1.5)
    ax.set_xticks(xticks)
    ax.set_xticklabels(xtick_labels)
    ax.set_yticks([])

    if legend == 'bottom':
        ax.legend(tags, loc='lower center', bbox_to_anchor=(0.5, 0),
            bbox_transform=ax.get_figure().transFigure, ncol=ncols)
        # ax.legend(tags, loc='upper right', bbox_to_anchor=(1, -0.1), ncol=ncols)
    elif legend == 'right':
        ax.legend(tags, loc='upper left', bbox_to_anchor=bbox_to_anchor,
            ncol=ncols)
    else:
        ax.legend().set_visible(False)

    plt.tight_layout()
