import logging
import os

import wandb

from pairwise_network import env, net


# -----------------------------------------------------------------------------
# Log
# -----------------------------------------------------------------------------
def log_summary(cfg, summary):
    log = {}
    if cfg['es_enabled']:
        for key, value in summary.items():
            log['es_' + key] = value
            if key in ['epoch', 'prune_iteration']:
                # log both es_<key> and <key> for wandb plots
                log[key] = value
    else:
        log = summary
    wandb.log(log)


# -----------------------------------------------------------------------------
# Save
# -----------------------------------------------------------------------------
def save_file(file):
    if os.path.isfile(file):
        logging.info('Saving %s to wandb' % file)
        wandb.save(file)
    else:
        logging.warning('File %s not found. Not uploading it to wandb.' % file)


def df_to_wandb_table(df):
    return wandb.Table(columns=list(df.columns), data=list(df.values))


def plt_to_wandb_image(plt):
    return wandb.Image(plt)


# -----------------------------------------------------------------------------
# Summary Nodes
# -----------------------------------------------------------------------------
def sn_table(group, update=False):
    filters = {}
    if group is not None:
        logging.info('Updating group %s' % group)
        filters = {'group': group}
    api = wandb.Api()
    runs = api.runs(path=env.wandb_project_path, filters=filters)
    for run in runs:
        logging.info('Loading run: %s.' % run.name)
        cfg = run.config
        use_cuda = cfg['device'] == 'cuda'

        # Seeds
        env.set_seeds(cfg['seed'])

        # Create Model
        model, pairwise_structure, mlp_mask = \
            net.create_pairwise_model_from_cfg(cfg)

        # Restore model parameters
        file = run.file(env.model_filename)
        file.download(replace=True, root=env.source_root_dir)
        model_filename = os.path.join(env.source_root_dir, env.model_filename)
        net.load_model_state_dict_from_file(model, model_filename, use_cuda)

        df = net.get_sn_weights_df(model,
            pairwise_structure)
        df = df.round(6)
        logging.info('Summary node weights: ')
        print(df.to_markdown())

        if update:
            run.summary.update({
                "table_sn": wandb.Table(columns=list(df.columns),
                    data=list(df.values))})


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
