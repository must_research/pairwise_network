import wandb

from pairwise_network import env


def update_run_cfg(run_path, parameter, value):
    api = wandb.Api()
    run = api.run(run_path)
    run.config[parameter] = value
    run.update()


def update_run_group_notes(run, group=None, notes=None):
    wandb.init(id=run.id, name=run.name, group=group, notes=notes, resume=True)


def update_runs_note(tag, note, append=True):
    api = wandb.Api()
    runs = api.runs(path=env.wandb_project_path)
    for run in runs:
        if run.state == "finished":
            if tag in run.tags:
                if append and run.notes is not None:
                    run.notes += ', ' + note
                else:
                    run.notes = note
                run.update()


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
