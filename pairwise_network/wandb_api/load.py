import logging
import os
import pickle
import torch
from sys import exit

from pairwise_network import config, env, net


# -----------------------------------------------------------------------------
# Model
# -----------------------------------------------------------------------------
def run_to_model(run, reload=True):
    logging.info('Loading run: %s.' % run.name)
    logging.info('Run ID: %s.' % str(run.id))
    cfg = run_to_cfg(run)
    use_cuda = cfg['device'] == 'cuda'

    # Seeds
    env.set_seeds(cfg['seed'])

    # Create model
    model, pairwise_structure, mlp_mask = net.create_pairwise_model_from_cfg(
        cfg)

    # Check paths
    run_out_dir = env.get_run_out_dir(run.id)
    model_file_path = os.path.join(run_out_dir, env.model_filename)
    mask_file_path = os.path.join(run_out_dir, env.mlp_mask_filename)
    if not os.path.isfile(model_file_path):
        reload = True
    elif not os.path.isfile(mask_file_path):
        reload = True

    # Download parameters and mask if needed
    if reload:
        for file in run.files():
            if file.name == env.model_filename:
                file.download(replace=True, root=run_out_dir)
            elif file.name == env.mlp_mask_filename:
                file.download(replace=True, root=run_out_dir)

    if os.path.isfile(mask_file_path):
        if use_cuda and not torch.cuda.is_available():
            logging.warning('Generating new mask since cuda is not available.')
        else:
            logging.info('Loading mask from %s' % mask_file_path)
            mlp_mask = pickle.load(open(mask_file_path, 'rb'))
    else:
        logging.warning(
            '%s not found generating new mlp mask.' % mask_file_path)

    if os.path.isfile(model_file_path):
        net.load_model_state_dict_from_file(model, model_file_path, use_cuda)
    else:
        logging.error('%s not found. Exiting.' % model_file_path)
        exit(101)

    net.prune_model_apply(model, mlp_mask)

    return model, pairwise_structure, mlp_mask


# -----------------------------------------------------------------------------
# Config
# -----------------------------------------------------------------------------
def run_to_cfg(run):
    cfg = config.ConfigDict()
    for key, value in run.config.items():
        if not key.startswith('_'):
            cfg[key] = value
    return cfg


def run_to_file_path(run, filename, reload=False):
    run_out_dir = env.get_run_out_dir(run.id)
    file_path = os.path.join(run_out_dir, filename)
    if not os.path.isfile(file_path):
        reload = True

    if reload:
        for file in run.files():
            if file.name == filename:
                file.download(replace=True, root=run_out_dir)

    return file_path


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
