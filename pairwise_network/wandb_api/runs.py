import logging

import pandas as pd
import wandb

import wandb_api
from pairwise_network import env, wandb_api


# -----------------------------------------------------------------------------
# Path, Id, Name
# -----------------------------------------------------------------------------
def run_path_to_name(run_path):
    run = wandb.Api().run(run_path)
    return run.name


def run_id_to_name(run_id):
    run_path = run_id_to_run_path(run_id)
    return run_path_to_name(run_path)


def run_id_to_run_path(run_id):
    return '/'.join([env.wandb_entity, env.wandb_project, run_id])


def run_id_to_run(run_id):
    run_path = run_id_to_run_path(run_id)
    return run_path_to_run(run_path)


def run_path_to_run(run_path):
    return wandb.Api().run(run_path)


# -----------------------------------------------------------------------------
# Filter, Sort
# -----------------------------------------------------------------------------
def get_runs_filtered_by_group(group):
    api = wandb.Api()
    runs = api.runs(path=env.wandb_project_path, filters={'group': group})
    return runs


def get_runs_filtered_by_tag(tags):
    """Retrieve wandb runs having the given tags.

    Args:
        tags (str, list): wandb run tags

    Returns:
        list: wandb runs

    """
    if type(tags) == str:
        tags = [tags, ]
    api = wandb.Api()
    logging.info('Wandb api request started.')
    runs = api.runs(path=env.wandb_project_path,
        filters={'tags': tags[0]})
    logging.info('Wandb api request finished.')
    runs_filtered = []
    del tags[0]
    for run in runs:
        all_tags_in_run = all((tag in run.tags for tag in tags))
        if all_tags_in_run:
            runs_filtered.append(run)
    logging.info('Retrieved %d runs' % len(runs_filtered))
    return runs_filtered


def filter_runs_by_cfg(runs, key, value):
    runs_filtered = []
    for run in runs:
        cfg = run.config
        if key in cfg.keys():
            if cfg[key] == value:
                runs_filtered.append(run)
    return runs_filtered


def filter_runs_by_tags(runs, tags_remove):
    filtered_runs = []
    for run in runs:
        remove_run = any((tag in run.tags for tag in tags_remove))
        if not remove_run:
            filtered_runs.append(run)
    runs = filtered_runs
    return runs


def filter_nan_from_runs(runs):
    filtered_runs = []
    for run in runs:
        run_summary = dict(run.summary)
        if run_summary['train_corr'] != 'NaN':
            filtered_runs.append(run)
    return filtered_runs


def sort_runs_by_name(runs):
    runs.sort(key=lambda run: run.name)


def sort_runs_by_summary_key(runs, key, reverse=True):
    runs.sort(key=lambda run: run.summary[key], reverse=reverse)


def sort_runs_by_config_key(runs, key, reverse=True):
    runs.sort(key=lambda run: run.config[key], reverse=reverse)


def sweep_to_runs(sweep_path):
    api = wandb.Api()
    sweep = api.sweep(sweep_path)
    runs = list(sweep.runs)
    return runs


# -----------------------------------------------------------------------------
# DataFrame
# -----------------------------------------------------------------------------
def runs_to_df(runs, filter_finished=True):
    """Creates a DataFrame from a list of wandb runs.

    Args:
        runs (list): wandb run objects.
        filter_finished (bool): if True, filters out runs that do not
            have a `finished` state.

    Returns:
        DataFrame: Every row is a run and  every column is either a config or
            a summary value.

    """
    records = []
    i = 1
    for run in runs:
        add_tag = '-'
        if run.state == 'finished' or not filter_finished:
            add_tag = '+'

            info = dict(run.summary)
            cfg = wandb_api.run_to_cfg(run)
            info.update(cfg)
            info['tags'] = run.tags
            info['state'] = run.state
            info['id'] = run.id
            info['name'] = run.name

            records.append(info)
        logging.info(f'({add_tag}) '
                     f'Run ({i}/{len(runs)}):\t'
                     f'State:{run.state}\t'
                     f'Name:{run.name}')
        i += 1

    df = pd.DataFrame(records)
    return df


def sweep_path_to_df(sweep_path):
    runs = sweep_to_runs(sweep_path)
    logging.info('%d runs found.' % len(runs))

    sweep_summary = runs_to_df(runs)

    return sweep_summary


def run_to_prune_df(run):
    history_df = run.history(samples=run.summary['_step'])
    prune_df = history_df.loc[history_df['prune_iteration'].dropna().index]
    prune_df = prune_df.loc[prune_df['valid_corr'].dropna().index]
    return prune_df


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
def filter_runs(runs, filter):
    filtered = runs.copy()
    for key, value in filter.items():
        filtered = wandb_api.filter_runs_by_cfg(filtered, key, value)
    return filtered