import wandb


def wandb_is_initialized():
    """Returns True if wandb is initialized."""
    return hasattr(wandb.config, '_wandb_dir')
