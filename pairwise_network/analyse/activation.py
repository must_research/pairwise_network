__author__ = 'jpbeukes27@gmail.com'
__description__ = 'Analyse pairwise activation values.'

import copy
import os
import re

import logging
import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from pairwise_network import env, dataset, net


def calc_sn_activation(model, x):
    """Calculates the summary node (sn) activation values of a pairwise \
    network for the given set of samples.

    References:
        mustnet3

    Args:
        model (Sequential): pairwise network
        x (Tensor):         input data

    Returns:
        ndarray: summary node activation values for every sample.

    """
    logging.debug('Calculating summary node activation values.')
    mlp_mask = net.prune_model_remove(model)

    # ========================================================================
    # Credit: This section is based on code from the mustnet3 package
    # ========================================================================
    model.eval()

    # Device
    if next(model.parameters()).is_cuda:
        if torch.cuda.is_available():
            x = x.to('cuda')
        else:
            # TODO: convert model to cpu
            logging.error('Cuda not available. Exiting.')
            exit(101)

    # Init
    all_results = {0: x}
    in_results = {}  # module output before activation function
    all_idx = 0
    in_idx = 0

    # Get the activation of each module
    for name, mod in model.named_children():
        all_idx += 1
        all_results[all_idx] = mod(all_results[all_idx - 1])
        regexp = re.compile(r'(lin_)')
        if regexp.search(name):
            in_idx += 1
            in_results[in_idx] = all_results[all_idx - 1].cpu().data.numpy()
    # ========================================================================

    n_layers = net.get_num_layers_from_model(model)
    sn_act = in_results[n_layers]

    net.prune_model_apply(model, mlp_mask)
    return sn_act


def multiply_sn_weights(model, sn_act):
    """Extracts summary node (sn) weights from model and multiply with sn \
    activation values.

    Args:
        model (Sequential): pairwise network.
        sn_act (ndarray):   activations.

    Returns:
        ndarray: Weighted summary node activations.

    """
    logging.debug('Multiplying sn activations by sn weights.')
    mlp_mask = net.prune_model_remove(model)
    sn_weights = net.get_sn_weights(model)
    sn_act_weighted = sn_act * sn_weights
    net.prune_model_apply(model, mlp_mask)
    return sn_act_weighted


def calc_plane_act(model, x1, x2, x1_tag, x2_tag, input_tags, subnet_idx,
        weighted=True):
    """Calculates the summary node (sn) activation for all combinations of \
    the provided input parameters.

    Args:
        model (Sequential): pairwise network
        x1 (ndarray):       n-dimensional vector
        x2 (ndarray):       m-dimensional vector
        x1_tag (str):       parameter 1 name
        x2_tag (str):       parameter 2 name
        input_tags (list):  input parameter tags
        subnet_idx (int):   summary node index of subnet
        weighted (bool):    if True, sn activation is scaled by sn weights

    Returns:
        ndarray: Matrix with shape (n, m): Activation values of subnet.

    """
    x1_idx = input_tags.index(x1_tag)
    x2_idx = input_tags.index(x2_tag)
    x = torch.zeros((len(x1), len(input_tags))).float()
    x[:, x1_idx] = torch.from_numpy(x1).float()
    result = np.zeros((len(x1), len(x2)), dtype=float)
    for i in range(len(x2)):
        x[:, x2_idx] = torch.full(x1.shape, x2[i]).float()
        act = calc_sn_activation(model, x)
        if weighted:
            act = multiply_sn_weights(model, act)
        result[:, i] = act[:, subnet_idx]
    return result


def load_sn_activation_df(x, model, pairwise_structure, file_path=None,
        reload_activations=False, weighted=True):
    """Calculates summary node (sn) activation values for a pairwise model on \
     the samples if the given file does not exist. Otherwise, this file is
     loaded and returned.

    Args:
        x (Tensor):                 input samples
        model (Sequential):         pairwise network.
        pairwise_structure (dict):  pairwise structure.
        file_path (str):            file where unweighted activation values are
            stored. If None, no activations are saved or retrieved, only
            recalculated.
        reload_activations (bool):  if True, activation values are
            recalculated regardless of whether the file exsist.
        weighted (bool):            if True, sn activations are scaled sn
            weights.

    Returns:
        DataFrame: Summary node activation values.

    """
    if file_path is None:
        logging.info('Calculating sn activations.')
        sn_act = calc_sn_activation(model, x)
        subnet_tags = list(pairwise_structure.keys())
        df_act = pd.DataFrame(data=sn_act, columns=subnet_tags)
    elif os.path.exists(file_path) and not reload_activations:
        logging.info(f'Loading unweighted sn activations from {file_path}.')
        df_act = pd.read_csv(file_path, index_col='index')
    else:
        logging.info('Reloading sn activations.')
        sn_act = calc_sn_activation(model, x)
        subnet_tags = list(pairwise_structure.keys())
        df_act = pd.DataFrame(data=sn_act, columns=subnet_tags)
        # (Assumes that summary nodes has the same order as the keys of the
        # pairwise structure)

        logging.info('Saving unweighted sn activation to %s.' % file_path)
        df_act.to_csv(file_path, index_label='index')

    if weighted:
        df_act.loc[:] = multiply_sn_weights(model, df_act.values)

    return df_act


def calc_sn_act_dist(df_act):
    """Calculate the summary node (sn) activation distribution from sn \
    activation values.
    
    Args:
        df_act (DataFrame): sn activation values.

    Returns:
        Series: Summary node activation distribution.

    """
    # 1) For each sample:
    #  1.1) Take the absolute value of the sn activation
    #  1.2) Divide each sn activation by the sum of all absolute sn activations
    # 2) Average over all samples
    logging.info('Calculating sn activation distribution.')
    df_abs = df_act.abs()
    df_sum = df_abs.sum(axis=1)
    df_ratio = df_abs.div(df_sum, axis=0)
    df_ratio_avg = df_ratio.mean(axis=0)
    return df_ratio_avg


def run_sn_act_dist(x, model, pairwise_structure, input_tags, file_path=None,
        reload_act=False, weighted=True, rename_subnets=True, add_star=True,
        return_renamed_tags=False, act_mask=None):
    """Calculate the summary node (sn) activation distribution and return as a
    Series. Renames subnets according to layer 2 weights, if specified.

    Args:
        x (Tensor):             input samples
        model (Sequential):     pairwise network.
        pairwise_structure (dict):  pairwise structure.
        partition (str):        partition of dataset to use (train, valid, eval)
        rename_subnets (bool):  if True subnets are renamed to reflect the
                                remaining input parameters, if some of the
                                input parameters are pruned away in layer 2
                                of the network.
        weighted (bool):        if True, sn activation is scaled by sn weights.
        reload_act (bool):      if True, activation values are recalculated.
        add_star (bool):        add an asterisk to subnet tags where only one
                                input parameter remains after pruning.
        return_renamed_tags (bool): if True, returns a list of the renamed
                                subnet tags. (Only available if `rename_subnets`
                                is True.)

    Returns:
        Series: Summary node activation distribution.

    """
    # Activation Values
    act_df = load_sn_activation_df(x, model, pairwise_structure, file_path,
        reload_activations=reload_act, weighted=weighted)

    if act_mask is not None:
        act_df['mask'] = act_mask
        act_df = act_df[act_df['mask'] == 1]
        act_df = act_df.drop(columns='mask')

    # Activation Distribution
    act_dist = calc_sn_act_dist(act_df)

    # Rename Subnets
    if rename_subnets:
        renamed_subnet_tags = net.rename_subnets_based_on_lin_2(model,
            pairwise_structure, input_tags)
        logging.info(f'Renaming subnets based on lin 2: {renamed_subnet_tags}')
        original_subnet_tags = list(pairwise_structure.keys())
        # Calculate act dist for subnets where only a single input remains.
        for param_tag in input_tags:
            tag = param_tag
            if add_star:
                tag += '*'
            act_dist[tag] = 0  # add parameter to distribution (init 0)
            for i in range(len(original_subnet_tags)):
                orig_tag = original_subnet_tags[i]
                ren_tag = renamed_subnet_tags[i]
                if param_tag == ren_tag:  # if param is the only subnet input
                    # sum all act distributions for this param
                    act_dist[tag] += act_dist[orig_tag]
                    # remove the original subnet's portion of act distribution
                    act_dist[orig_tag] = 0

        if return_renamed_tags:
            return act_dist, renamed_subnet_tags

    return act_dist


# -----------------------------------------------------------------------------
# Plot
# -----------------------------------------------------------------------------
def plot_act_dist(distributions, ax=None, cmap=None, width=0.8):
    """Plots stacked bar chart of several distributions.

    Args:
        distributions (list, DataFrame): distributions. If a list of Series
            is given, the distributions are concatenated.

    """
    if type(distributions) == list:
        df = pd.concat(distributions, axis=1)
    else:
        df = distributions
    df.transpose().plot.bar(stacked=True, ax=ax, rot=0, cmap=cmap, width=width)


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
