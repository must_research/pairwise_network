__author__ = 'slotz@sansa.org.za, jpbeukes27@gmail.com'
__description__ = 'Create, train and do input parameter selection with ' \
                  'a pairwise neural network (Lotz et al. FAIR 2019)'

import os
import logging
import time

import pandas as pd
import numpy as np
import copy
import pickle
import argparse
from collections import OrderedDict

import torch

from pairwise_network import metrics, env, net, config, dataset
from pairwise_network.analyse.activation import run_sn_act_dist


# -----------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(
        description='Train, test and get sum of weights for pairwise networks.')

    subparsers = parser.add_subparsers(title='sub-commands',
        help='select command to execute',
        dest='action')

    train_parser = subparsers.add_parser('train',
        help='train a pairwise network.')
    # ------
    # config
    # ------
    train_parser.add_argument('config', type=str, help='config file.')
    # --------------------------
    # alternative config options
    # --------------------------
    train_parser.add_argument('-d', '--dataset', type=str, default=None,
        required=False, help='dataset name.')
    train_parser.add_argument('-m', '--model_params', type=str, default=None,
        required=False, help='parameter file')
    train_parser.add_argument('-s', '--seed', type=int, default=None,
        required=False, help='initialization seed')
    train_parser.add_argument('-b', '--batch', type=int, default=None,
        required=False, help='mini-batch size')
    train_parser.add_argument('-l', '--lr', type=float, default=None,
        required=False, help='learning rate size')
    train_parser.add_argument('-y', '--art_function', type=str, default=None,
        required=False, help='change y function of artificial dataset.')
    train_parser.add_argument('-r', '--reload', action='store_true',
        required=False, help='reload artificial dataset.')
    # -------------
    # wandb options
    # -------------
    train_parser.add_argument('--wandb', action='store_true',
        required=False, help='use wandb.')
    train_parser.add_argument('--sweep', action='store_true',
        required=False, help='use wandb sweep.')
    train_parser.add_argument('-p', '--project', type=str,
        default=env.wandb_project,
        required=False, help='overrides wandb project name in env')
    train_parser.add_argument('-g', '--group', type=str, default=None,
        required=False, help='a string by which to group experiments.')
    train_parser.add_argument('-n', '--name', type=str, default=None,
        required=False, help='wandb run name.')
    train_parser.add_argument('-N', '--notes', type=str, default=None,
        required=False, help='wandb run notes.')
    train_parser.add_argument('-t', '--tags', type=str, default=None,
        required=False, nargs='*',
        help='a list of strings to associate with this experiment.')

    # train_further_parser = subparsers.add_parser('resume',
    #     help='continue training an existing pairwise network.')
    # train_further_parser.add_argument('run_path', type=str,
    #     help='wandb run path.')
    # train_further_parser.add_argument('-e', '--epochs', type=int, default=0,
    #     required=False, help='epochs to add.')

    parser.add_argument('--debug', action='store_true',
        help='change logging mode to DEBUG; default is INFO')

    args = parser.parse_args()

    log_level = 'INFO'
    if args.debug:
        log_level = 'DEBUG'
    env.set_log_level(log_level)

    if args.action == 'train':
        cfg = config.cfg_from_yaml(args.config)
        if args.seed is not None:
            cfg['seed'] = int(args.seed)
        if args.dataset is not None:
            cfg['dataset'] = args.dataset
        if args.batch is not None:
            cfg['batch_size'] = args.batch
        if args.lr is not None:
            cfg['lr'] = args.lr
        if args.art_function is not None:
            cfg['art_function'] = args.art_function
        run_train(project=args.project, tags=args.tags, group=args.group,
            cfg=cfg, run_name=args.name, use_wandb=args.wandb,
            run_notes=args.notes, reload_dataset=args.reload, sweep=args.sweep)
    # elif args.action == 'resume':
    #     run_train_resume(args.run_path, args.epochs)

    logging.info('Done.')


# -----------------------------------------------------------------------------
# Run
# -----------------------------------------------------------------------------
# def run_train_resume(run_path, epochs_added=0, model=None, mlp_mask=None):
#     # TODO: Check previous es_valid_corr and es_valid_train
#     wandb_api = wandb.Api()
#     run = wandb_api.run(run_path)
#     wandb.init(project=run.project, entity=run.entity, id=run.id, name=run.name,
#         resume=True)
#     cfg = wandb.config
#
#     epoch_start = cfg['max_epochs']
#     if epochs_added > 0:
#         cfg.update({'max_epochs': cfg['max_epochs'] + epochs_added},
#             allow_val_change=True)
#
#     # Seeds
#     env.set_seeds(cfg['seed'])
#
#     # Create Model
#     if model is None:
#         model, pairwise_structure, mlp_mask = net.create_pairwise_model_from_cfg(
#             cfg)
#     elif mlp_mask is None:
#         logging.error('mlp_mask must be provided if model is given.')
#         exit(101)
#     else:
#         pickle.dump(mlp_mask,
#             open(os.path.join(wandb.run.dir, env.model_mlp_mask), 'wb'))
#
#     # Set Workers
#     torch.set_num_threads(env.max_num_threads)
#
#     # Restore model parameters
#     model_params = wandb.restore(env.model)
#     # net.load
#     net.load_model_state_dict_from_file(model, model_params.name)
#     # model.load_state_dict(torch.load(model_params.name))
#
#     # Optimizer and Loss Function
#     loss_fn = get_loss_function_from_cfg(cfg)
#     optimizer = get_optimizer_from_cfg(cfg, model)
#
#     # Data Loaders
#     dataloaders = dataset.create_dataloaders(cfg)
#
#     # Train
#     model_state_dict, epoch, _, _ = train_model(cfg, model, dataloaders,
#         loss_fn, optimizer,
#         epoch_start)  # TODO: add train_corr and valid_corr
#     net.load_model_state_dict(model, model_state_dict)
#
#     # Evaluate
#     evaluation_result, evaluation_result_df = evaluate_model(cfg, model,
#         dataloaders, loss_fn)
#     print(evaluation_result_df.to_markdown())
#     summary = {'epoch': epoch}
#     summary.update(evaluation_result)
#     api.log_summary(cfg, summary)


def run_train(project='debug', tags=None, group=None, cfg=None, run_name=None,
        use_wandb=False, reinit_wandb=False, run_notes=None,
        reload_dataset=False, es_on_loss=True, sweep=False):
    """Train and evaluate a pairwise network.

    Args:
        reload_dataset: reloads artificial datasets if True.
        tags (list): identifiers for associating sets of experiments.

    """

    if use_wandb:
        # Init wandb
        import wandb
        env.enable_wandb()
        if project is None:
            project = env.wandb_project
        wandb.init(
            project=project,
            tags=tags,
            group=group,
            name=run_name,
            reinit=reinit_wandb,
            notes=run_notes
        )

        if sweep:
            cfg = config.update_cfg_with_wandb_cfg(cfg, wandb.config)

        wandb.config.update(cfg, allow_val_change=True)

    device = env.set_device(cfg['device'])
    cfg['device'] = device

    # Seeds
    env.set_seeds(cfg['seed'])

    # Create Model
    model, pairwise_structure, best_mlp_mask = \
        net.create_pairwise_model_from_cfg(cfg)

    # Set Workers
    torch.set_num_threads(env.max_num_threads)

    # Init Model
    init_model(model, pairwise_structure, cfg['init_scheme'], cfg['activation'])
    init_state_dict = net.get_model_state_dict(model)

    # Optimizer and Loss Function
    loss_fn = get_loss_function(cfg['loss'])
    optimizer = get_optimizer(model, cfg['optimizer'], cfg['lr'],
        cfg['adam_beta1'], cfg['adam_beta2'], cfg['weight_decay'])

    # Dataloaders
    dataloaders, input_tags = dataset.create_dataloaders_from_cfg(cfg,
        reload_dataset)

    # Train  TODO: Merge train and prune functions
    best_state_dict, best_epoch, max_epoch = train_model(model, dataloaders,
        loss_fn, optimizer, cfg['max_epoch'], use_wandb=use_wandb,
        tolerance=cfg['tolerance'], patience=cfg['patience'], device=device,
        es_enabled=cfg['es_enabled'], des_check_perc=cfg['es_check_perc'],
        des_step_perc=cfg['es_step_perc'], es_on_loss=es_on_loss,
        max_epoch_patience=cfg['max_epoch_patience'])

    # Iteratively Prune and Train
    best_prune_iteration = 0
    if 0 < cfg['prune_iterations_max']:
        best_state_dict, best_mlp_mask, best_prune_iteration, \
        best_epoch, max_epoch = prune_train(model, dataloaders, loss_fn,
            optimizer, max_epoch=max_epoch, start_epoch=best_epoch,
            use_wandb=use_wandb, pairwise_structure=pairwise_structure,
            start_mlp_mask=best_mlp_mask, init_state_dict=init_state_dict,
            start_state_dict=best_state_dict, device=device,
            train_tolerance=cfg['tolerance'], train_patience=cfg['patience'],
            es_enabled=cfg['es_enabled'], des_check_perc=cfg['es_check_perc'],
            des_step_perc=cfg['es_step_perc'],
            prune_iterations_max=cfg['prune_iterations_max'],
            prune_amount=cfg['prune_amount'], prune_type=cfg['prune_type'],
            prune_tolerance=cfg['prune_tolerance'],
            prune_epochs=cfg['prune_epochs'],
            prune_init_method=cfg['prune_init_method'], input_tags=input_tags,
            es_on_loss=es_on_loss,
            max_epoch_patience=cfg['max_epoch_patience'],
            prune_constant_epochs=cfg['prune_constant_epochs'],
        )
    cfg['max_epoch'] = max_epoch

    # Evaluate
    net.prune_model_remove(model)
    net.prune_model_apply(model, best_mlp_mask)
    net.load_model_state_dict(model, best_state_dict)
    evaluation_result, evaluation_result_df = evaluate_model(model, dataloaders,
        loss_fn, device)
    logging.info(f'Final Performance (iteration {best_prune_iteration}, '
                 f'epoch {best_epoch}):\n{evaluation_result_df.to_markdown()}')

    # Log and save to wandb
    if use_wandb:
        global_sparsity, _, _ = net.get_model_sparsity(model,
            pairwise_structure)
        import wandb
        from pairwise_network import wandb_api
        summary = {
            'epoch': best_epoch,
            'prune_iteration': best_prune_iteration,
            'global_sparsity': global_sparsity,
        }
        summary.update(evaluation_result)
        # es_ prefix will be added to the summary keys
        wandb_api.log_summary(cfg, summary)
        run_dir = wandb.run.dir
        torch.save(best_state_dict, os.path.join(run_dir, env.model_filename))
        pickle.dump(best_mlp_mask,
            open(os.path.join(run_dir, env.mlp_mask_filename), 'wb'))
        pickle.dump(pairwise_structure,
            open(os.path.join(run_dir, env.pairwise_structure_filename), 'wb'))

    return best_state_dict, pairwise_structure, best_mlp_mask


def prune_train(model, dataloaders, loss_fn, optimizer, max_epoch, start_epoch,
        use_wandb, pairwise_structure, start_mlp_mask, init_state_dict,
        start_state_dict, device='cpu', train_tolerance=0.00001,
        train_patience=100, es_enabled=True, des_check_perc=30,
        des_step_perc=30, prune_iterations_max=0, prune_amount=20,
        prune_type='l1_unstructured', prune_tolerance=1, prune_epochs=50,
        prune_init_method='continue', input_tags=None, es_on_loss=True,
        max_epoch_patience=0, prune_constant_epochs=True):
    """Iteratively prune and train a pairwise network.

    Args:
        prune_constant_epochs:
    """
    # Activation distribution (initial)
    net.load_model_state_dict(model, start_state_dict)
    act_dist_list = [run_sn_act_dist(dataloaders[1].dataset.data, model,
            pairwise_structure, input_tags), ]
    # prune_save = dict(
    #     act_dist=act_dist_list.copy(),
    #     state_dict=[start_state_dict.copy(), ],
    #     mlp_mask=[start_mlp_mask.copy(), ]
    # )



    evaluation_result, evaluation_result_df = evaluate_model(model,
        dataloaders, loss_fn, device)
    logging.info('Performance before prune iterations:\n' +
                 evaluation_result_df.to_markdown())

    best_iteration = optim_iteration = iteration = 0
    best_epoch = optim_epoch = epoch = start_epoch
    best_results = evaluation_result.copy()
    optim_mlp_mask = start_mlp_mask.copy()
    optim_state_dict = start_state_dict.copy()
    performance_metric = 'loss' if es_on_loss else 'corr'

    # Log
    if use_wandb:
        sparsity, _, _ = net.get_model_sparsity(model, pairwise_structure)
        import wandb
        summary = {
            'prune_iteration': iteration,
            'global_sparsity': sparsity,
        }
        summary.update(evaluation_result)
        wandb.log(summary)

    # Iterative pruning loop
    pruning = iteration < prune_iterations_max
    while pruning:
        iteration += 1
        logging.info('-- Start prune iteration %d --' % iteration)
        if iteration >= prune_iterations_max:
            pruning = False

        # Init
        if prune_init_method == 'reset':
            logging.info('Loading original state dict (resetting unpruned '
                         'weights to original init).')
            net.load_model_state_dict(model, init_state_dict)

        # Prune
        sparsity, _, _ = net.get_model_sparsity(model, pairwise_structure)
        logging.info(f'Global sparsity before pruning: {sparsity:.4f}')
        net.prune_per_layer(model, prune_amount, prune_type)
        sparsity, _, _ = net.get_model_sparsity(
            model, pairwise_structure)
        logging.info(f'Global sparsity after pruning: {sparsity:.4f}')

        # Train
        # TODO: Reset optimizer every iteration?
        if prune_constant_epochs:
            max_epoch = epoch + prune_epochs
            des_check_perc = 0
            des_step_perc = 0
        else:
            max_epoch += prune_epochs
        state_dict, epoch, _ = train_model(model, dataloaders, loss_fn,
            optimizer, max_epoch=max_epoch, start_epoch=epoch,
            use_wandb=use_wandb, best_epoch=epoch, tolerance=train_tolerance,
            patience=train_patience, device=device, es_enabled=es_enabled,
            des_check_perc=des_check_perc, des_step_perc=des_step_perc,
            es_on_loss=es_on_loss, max_epoch_patience=max_epoch_patience)
        net.load_model_state_dict(model, state_dict)
        mlp_mask = net.get_mlp_mask_from_model(model)

        # Activation distribution
        d = run_sn_act_dist(dataloaders[0].dataset.data, model,
            pairwise_structure, input_tags)
        act_dist_list.append(d)
        # prune_save['act_dist'].append(d)
        # prune_save['state_dict'].append(state_dict.copy())
        # prune_save['mlp_mask'].append(mlp_mask.copy())

        # Evaluate pruning iteration
        evaluation_result, evaluation_result_df = evaluate_model(model,
            dataloaders, loss_fn, device)
        logging.info('Performance:\n' + evaluation_result_df.to_markdown())
        logging.info('-- End prune iteration %d --' % iteration)
        # Log
        if use_wandb:
            import wandb
            summary = {
                'epoch': epoch,
                'prune_iteration': iteration,
                'global_sparsity': sparsity,
            }
            summary.update(evaluation_result)
            wandb.log(summary)

        # Early Stopping
        # Check if performance falls below threshold
        if es_on_loss:  # Early stop on loss
            performance = evaluation_result['valid_loss']
            best_performance = best_results['valid_loss']
            threshold = best_performance + prune_tolerance
            is_optim_iteration = performance < threshold
            is_best_iteration = performance < best_performance
        else:  # Early stop on corr
            performance = evaluation_result['valid_corr']
            best_performance = best_results['valid_corr']
            threshold = best_performance - prune_tolerance
            is_optim_iteration = performance > threshold
            is_best_iteration = performance > best_performance
        if is_optim_iteration:  # Optimal iteration
            optim_state_dict = state_dict
            optim_mlp_mask = mlp_mask
            optim_epoch = epoch
            optim_iteration = iteration
            if is_best_iteration:  # Best performing iteration
                best_results = evaluation_result.copy()
                best_iteration = iteration
                best_epoch = epoch
        else:
            logging.info('Pruning tolerance exceeded:')
            logging.info(f'Best {performance_metric}: {best_performance:.4f}, '
                         f'iteration:{best_iteration}, '
                         f'epoch: {best_epoch}')
            logging.info(f'Last {performance_metric}: {performance:.4f}, '
                         f'iteration:{iteration}, '
                         f'epoch: {epoch}')
            pruning = False

    if use_wandb:
        import wandb
        file_path = os.path.join(wandb.run.dir, env.act_dist_filename)
        pickle.dump(act_dist_list, open(file_path, 'wb'))
        # file_path = os.path.join(wandb.run.dir, env.prune_save_filename)
        # pickle.dump(prune_save, open(file_path, 'wb'))

    return optim_state_dict, optim_mlp_mask, optim_iteration, optim_epoch, max_epoch


# -----------------------------------------------------------------------------
# Loss, Optimizer, Scheduler
# -----------------------------------------------------------------------------
def get_loss_function(loss_fn_tag):
    loss_fn = None
    if loss_fn_tag == 'mse':
        loss_fn = torch.nn.MSELoss()
    else:
        logging.error('Unknown loss function: %s', loss_fn_tag)
        exit(101)
    return loss_fn


def get_optimizer(model, optimizer_tag, lr, adam_beta1=0.9,
        adam_beta2=0.99, weight_decay=0):
    optimizer = None
    if optimizer_tag == 'adam':
        optimizer = torch.optim.Adam(
            model.parameters(),
            lr=lr,
            betas=(adam_beta1, adam_beta2),
            weight_decay=weight_decay
        )
    else:
        logging.error('Unknown optimizer: %s', optimizer_tag)
        exit(101)
    return optimizer


# -----------------------------------------------------------------------------
# Init, Seeds
# -----------------------------------------------------------------------------
def init_model(model, pairwise_structure, init_scheme, activation):
    mlp_mask = net.prune_model_remove(model)  # remove parameter buffers
    for name, param in model.named_parameters():
        if 'bn' in name or 'bias' in name or 'lin_1' in name:
            continue  # ignore batch norm, bias and input layer
        else:
            if init_scheme in ('random', 'identical_subnets'):
                torch.nn.init.kaiming_uniform_(param,
                    torch.nn.init.calculate_gain(activation))
                logging.info('%s: Using kaiming/He init scheme.' % name)
    net.prune_model_apply(model, mlp_mask)  # add parameter buffers

    if init_scheme == 'identical_subnets':
        logging.info('Initializing each subnet identically.')
        init_model_identical_subnets(model, pairwise_structure)


def init_model_identical_subnets(model, pairwise_structure):
    w_pairwise = net.get_pairwise_weights(model, pairwise_structure)
    w_subnet_first = copy.deepcopy(next(iter(w_pairwise.values())))
    for w_subnet in w_pairwise.values():
        for layer in range(2, len(w_subnet) + 1):
            w_subnet[layer] = w_subnet_first[layer]

    net.set_pairwise_weights(model, pairwise_structure, w_pairwise)


def init_ones(model):
    """Initializes all model parameters with a value of one."""
    mlp_mask = net.prune_model_remove(model)
    for name, module in model.named_modules():
        for param in module.parameters():
            torch.nn.init.ones_(param)
    net.prune_model_apply(model, mlp_mask)


# -----------------------------------------------------------------------------
# Train, Rate, Evaluate
# -----------------------------------------------------------------------------
def train_model(model, dataloaders, loss_fn, optimizer, max_epoch,
        start_epoch=0, use_wandb=False, best_epoch=0, tolerance=0.00001,
        patience=100, device='cpu', es_enabled=True, des_check_perc=30,
        des_step_perc=30, es_on_loss=True, max_epoch_patience=-1):
    """Trains a pairwise network"""
    absolute_max_epoch = max_epoch + max_epoch_patience
    performance_metric = 'loss' if es_on_loss else 'corr'
    n_inputs = net.input_dim(model)

    best_model_state_dict = net.get_model_state_dict(model)
    if es_on_loss:  # Early stop on loss
        best_performance = last_performance = np.inf
    else:  # Early stop on corr
        best_performance = last_performance = -1
    epoch = start_epoch

    patience_count = 0

    train_loader, valid_loader, _ = dataloaders
    batch_size = train_loader.batch_size

    logging.info('Started Training')

    train_start_time = time.perf_counter()

    log = get_log_dict()
    print_log(log, head=True)

    training = True
    while training:
        epoch_start_time = time.perf_counter()
        epoch += 1
        patience_count += 1
        model.train()
        for batch_idx, (x, y) in enumerate(train_loader):
            # Dimensions
            x = x.view(batch_size, n_inputs)
            # Device
            x = x.to(device)
            y = y.to(device)

            # Forward pass
            optimizer.zero_grad()
            y_pred = model.forward(x)
            # Loss
            loss = loss_fn(y_pred, y)
            # Gradient
            loss.backward()
            # Update
            optimizer.step()

        # Rate
        train_corr, train_loss = rate_model(model, train_loader, loss_fn,
            device)
        valid_corr, valid_loss = rate_model(model, valid_loader, loss_fn,
            device)

        epoch_end_time = time.perf_counter()

        # Log
        log['epoch'] = epoch
        log['valid_corr'] = valid_corr
        log['valid_loss'] = valid_loss
        log['train_corr'] = train_corr
        log['train_loss'] = train_loss
        log['cp'] = False
        log['dur'] = format_timedelta(epoch_end_time - epoch_start_time)

        if use_wandb:
            import wandb
            log_wandb = copy.deepcopy(log)
            log_wandb.pop('cp')
            log_wandb.pop('dur')
            wandb.log(log_wandb)

        # Calc early stopping regardless of whether it is enabled
        if es_on_loss:  # Early stop on loss
            performance = valid_loss
            is_best_epoch = performance < best_performance
        else:  # Early stop on corr
            performance = valid_corr
            is_best_epoch = performance > best_performance
        if is_best_epoch:
            best_model_state_dict = net.get_model_state_dict(model)
            if use_wandb:
                import wandb
                file_path = os.path.join(wandb.run.dir, env.model_filename)
                torch.save(best_model_state_dict, file_path)
            best_epoch = epoch
            best_performance = performance
            log['cp'] = True

        print_log(log)

        # Stopping Criteria
        if epoch == max_epoch:
            check_range = max_epoch * (1. - des_check_perc / 100.)
            if best_epoch > check_range and es_enabled:
                step_range = int(max_epoch * des_step_perc / 100.)
                step_range = 1 if step_range < 1 else step_range
                logging.warning('Unconvincing convergence (best model at '
                                'epoch ' + str(best_epoch) + '), adding '
                                + str(step_range) + ' epochs.')
                max_epoch += step_range
            else:
                logging.info('Max number of epochs reached.')
                training = False
        elif max_epoch_patience > -1 and epoch >= absolute_max_epoch:
            logging.info('Absolute max number of epochs reached.')
            training = False
        # Check tolerance (set to -1 to disable)
        elif abs(performance - last_performance) < tolerance:
            if patience_count >= patience:
                logging.info(f'Tolerance of {tolerance} exceeded on validation '
                             f'{performance_metric} (old {last_performance:.7f}'
                             f' vs new {performance:.7f})')
                training = False
        else:
            patience_count = 0
            last_performance = valid_loss if es_on_loss else valid_corr

    train_end_time = time.perf_counter()
    logging.info('Finished Training. Took %s' % format_timedelta(
        train_end_time - train_start_time))

    if es_enabled:
        return best_model_state_dict, best_epoch, max_epoch
    else:
        model_state_dict = net.get_model_state_dict(model)
        if use_wandb:
            import wandb
            torch.save(model_state_dict,
                os.path.join(wandb.run.dir, env.model_filename))
        return model_state_dict, epoch, max_epoch


def rate_model(model, dataloader, loss_fn=None, device='cpu'):
    loss_batch = []
    model.eval()
    y_true = np.array([])
    y_pred = np.array([])
    n_inputs = net.input_dim(model)
    for batch_idx, (x, y) in enumerate(dataloader):
        x = x.view(-1, n_inputs)
        x = x.to(device)
        y = y.to(device)
        y_pred_batch = model.forward(x)
        if loss_fn:
            loss = loss_fn(y_pred_batch, y)
            loss_batch.append(float(loss))
        y_pred = np.append(y_pred, y_pred_batch.data.cpu().numpy())
        y_true = np.append(y_true, y.cpu().numpy())
    corr = metrics.correlation(y_pred, y_true)
    if loss_fn:
        epoch_loss = sum(loss_batch) / float(len(loss_batch))
        return corr, epoch_loss
    else:
        return corr


def evaluate_model(model, dataloaders, loss_fn=None, device='cpu'):
    result = {}
    partitions = ['train', 'valid', 'eval']
    result_df = pd.DataFrame(columns=partitions,
        index=['corr', 'loss'])
    for i in range(len(dataloaders)):
        dataloader = dataloaders[i]
        partition = partitions[i]
        corr = None
        loss = None
        if dataloader is not None:
            if loss_fn:
                corr, loss = rate_model(model, dataloader, loss_fn, device)
            else:
                corr = rate_model(model, dataloader, device=device)
        result_df[partition]['corr'] = corr
        result_df[partition]['loss'] = loss
        result[partition + '_corr'] = corr
        result[partition + '_loss'] = loss
    return result, result_df


# -----------------------------------------------------------------------------
# Log
# -----------------------------------------------------------------------------
def get_log_dict(using_valid_set=True):
    log = OrderedDict()
    log['epoch'] = None
    log['train_corr'] = None
    log['train_loss'] = None
    if using_valid_set:
        log['valid_corr'] = None
        log['valid_loss'] = None
    log['cp'] = None
    log['dur'] = None
    return log


def print_log(log, head=False):
    def get_log_spacing(col):
        if col == 'cp':
            return 6
        else:
            return 15

    col_format = ''
    if head:
        head_line = ''
        for col in log:
            col_spacing = get_log_spacing(col)
            head_line += '  ' + '-' * (col_spacing - 2)
            col_format += '{:>%ds}' % col_spacing
        logging.info(col_format.format(*log))
        logging.info(head_line)
    else:
        for col, value in log.items():
            if 'corr' in col or 'loss' in col:
                log[col] = str('%.6f' % value)
            elif col == 'cp':
                log[col] = '' if value is False else '+'
            else:
                log[col] = str(value)

            col_spacing = get_log_spacing(col)
            col_format += '{:>%ds}' % col_spacing
        logging.info(col_format.format(*log.values()))


def format_timedelta(seconds):
    days, remainder = divmod(seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)
    return '{:2}d {:02}:{:02}:{:02}'.format(int(days), int(hours), int(minutes),
        int(seconds))


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main()
# -----------------------------------------------------------------------------
