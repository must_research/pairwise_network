__author__ = 'jpbeukes27@gmail.com'

import pandas as pd
from sklearn import preprocessing

import torch
import torch.utils.data

import logging
import os
from sys import exit

from pairwise_network import env
from pairwise_network.art.art_generate import create_art_dataset


# -----------------------------------------------------------------------------
# Create
# -----------------------------------------------------------------------------
def create_dataloaders_from_cfg(cfg, reload=False):
    """Creates a data loaders from the specifications of a config object.

        Args:
            reload (bool): reloads artificial datasets if True.
            cfg (ConfigDict): config

        Returns:
            list: Torch data loaders (train, valid, eval)

    """
    datasets, input_tags = create_dataset_from_cfg(cfg, reload=reload)
    dataloaders = create_dataloaders(cfg['batch_size'], datasets)
    return dataloaders, input_tags


def create_dataset_from_cfg(cfg, reload=False, data_dir=None):
    """Creates a dataset from the specifications of a config object.

        Args:
            reload (bool): reloads artificial datasets if True.
            cfg (ConfigDict): config

        Returns:
            list: (Torch datasets (train, valid, eval), list of input tags)

    """
    datasets = list()
    dataset_name = cfg['dataset']
    input_tags = cfg['inputs']
    if data_dir is None:
        data_dir = env.data_dir
    if 'symh' in dataset_name:
        import symh_dataset
        datasets, columns = symh_dataset.create_datasets(
            dataset_name,
            data_dir=data_dir,
            include_phase=cfg['phase'] == 'All',
            timeshift=cfg['timeshift'],
            event_sensitive=True,
            interpol_limit=10,
            standardize=True,
            return_columns=True
        )
        input_tags = columns[:-1]
        # TODO: Add event_sensitive and interpol to cfg
    elif 'eps' in dataset_name:
        env.set_seeds(1)
        datasets = create_eps_dataset(dataset_name, data_dir, cfg['inputs'],
            cfg['outputs'], cfg['standardize_dataset'])
        env.set_seeds(cfg['seed'])
    elif dataset_name.startswith('test'):
        datasets = create_test_dataset(dataset_name, cfg['inputs'],
            cfg['outputs']
        )
    elif dataset_name.startswith('art'):
        df_datasets = create_art_dataset(cfg, reload, data_dir)
        datasets = df_datasets_to_torch(df_datasets, cfg['inputs'],
            cfg['outputs'], cfg['standardize_dataset'])
    else:
        logging.error(f'Dataset {dataset_name} not implemented.')
        exit(101)

    return datasets, input_tags


def datasets_to_partition(datasets, partition):
    """Extracts a partition from the list of datasets. Exist if the \
    partition tag or dataset does not exist.

    Args:
        datasets (list, tuple):    list of 3 items
        partition (str):    partition to return (train, valid, eval)

    Returns:
        CustomDataset: Torch dataset.

    """
    if partition not in env.partitions:
        logging.error(f'Partition {partition} not in {str(env.partitions)}.')
        exit(101)
    idx = env.partitions.index(partition)
    result = datasets[idx]
    if result is None:
        logging.error(f'Partition {partition} not in list of datasets.')
        exit(101)
    return result


def create_dataloaders(batch_size, datasets):
    """Create dataloaders from a list of datasets. The last batch of the \
    train loader is dropped, but not vor the valid and eval loaders. A \
    train dataset is required, but the validation and evaluation sets are \
    optional and can be replaced with a None. No shuffling is applied to any \
    of the loaders.

    """
    dataloaders = [
        torch.utils.data.DataLoader(
            dataset=datasets[0],
            batch_size=batch_size,
            shuffle=False,
            drop_last=True,
            num_workers=env.num_dataloader_workers
        )  # training loader
    ]

    for dataset in datasets[1:]:  # validation & evaluation loaders
        if dataset is not None:
            dataloaders.append(
                torch.utils.data.DataLoader(
                    dataset=dataset, batch_size=batch_size, shuffle=False,
                    drop_last=False, num_workers=env.num_dataloader_workers
                )
            )
        else:
            dataloaders.append(None)

    return dataloaders


def df_datasets_to_torch(df_datasets, inputs_tags, outputs_tags,
        standardize=True):
    if standardize:
        df_datasets = scale_df_datasets(df_datasets, inputs_tags, outputs_tags)
    datasets = list()
    for df in df_datasets:
        if df is not None:
            x_values = df[inputs_tags].values
            y_values = df[outputs_tags].values

            x_tensor = torch.tensor(x_values).type(torch.FloatTensor)
            y_tensor = torch.tensor(y_values).type(torch.FloatTensor)

            datasets.append(CustomDataset(x_tensor, y_tensor))
        else:
            datasets.append(None)

    return datasets


def scale_df_datasets(df_datasets, intput_tags, output_tags):
    x_scaler, y_scaler = df_to_scaler(df_datasets[0], intput_tags, output_tags)
    for df in df_datasets:
        if df is not None:
            x_values = df[intput_tags].values
            y_values = df[output_tags].values
            x_scaled = x_scaler.transform(x_values)
            y_scaled = y_scaler.transform(y_values)
            df[intput_tags] = x_scaled
            df[output_tags] = y_scaled
    return df_datasets


def df_to_scaler(df, intput_tags, output_tags):
    x_values = df[intput_tags].values
    y_values = df[output_tags].values
    x_scaler = preprocessing.StandardScaler().fit(x_values)
    y_scaler = preprocessing.StandardScaler().fit(y_values)
    return x_scaler, y_scaler


def read_df_datasets(dataset_name, data_dir=None):
    df_datasets = list()
    for partition in ('train', 'valid', 'eval'):
        df = read_df(partition, dataset_name, data_dir)
        df_datasets.append(df)
    return df_datasets


def read_df(partition, dataset_name, data_dir=None):
    data_path = ''
    if dataset_name in ('epsv2', 'epsv3', 'epsv4', 'epsv5'):
        data_path = os.path.join(data_dir, dataset_name, partition + '.pkl')
    else:
        logging.error(f'Dataset {dataset_name} not found.')
        exit(101)

    logging.info('Loading %s' % data_path)
    df = pd.read_pickle(data_path)
    return df


# -----------------------------------------------------------------------------
# Dataset: Test
# -----------------------------------------------------------------------------
def create_test_dataset(dataset_name, input_tags, output_tags):
    n_inputs = len(input_tags)
    n_outputs = len(output_tags)

    x = torch.zeros(n_inputs)  # default: zeros
    y = torch.zeros(n_outputs)  # default: zeros

    if 'ones' in dataset_name:
        x = torch.ones(n_inputs)
        y = torch.ones(n_outputs)
    elif 'random' in dataset_name:
        x = torch.rand(n_inputs)
        y = torch.rand(n_outputs)

    datasets = {
        'train': CustomDataset(
            x.type(torch.FloatTensor),
            y.type(torch.FloatTensor)
        ),
        'valid': None,
        'eval': None,
    }
    logging.info('Created custom test dataset for verification:')
    logging.info('x: %s' % str(x))
    logging.info('y: %s' % str(y))

    return datasets


# -----------------------------------------------------------------------------
# Dataset: Epsilon
# -----------------------------------------------------------------------------
def create_eps_dataset(dataset_name, data_dir, input_tags, output_tags,
        standardize):
    df_datasets = read_df_datasets(dataset_name, data_dir)
    datasets = df_datasets_to_torch(df_datasets, input_tags,
        output_tags, standardize=standardize)
    return datasets


# -----------------------------------------------------------------------------
# Copied from mustnet
# -----------------------------------------------------------------------------
class CustomDataset(torch.utils.data.Dataset):
    """Create a custom dataset, given a tensor of values and a tensor of
    labels.

    References:
        mustnet3

    """

    def __init__(self, x, y, x_transform=None, y_transform=None):
        self.data = x
        self.targets = y
        self.x_transform = x_transform
        self.y_transform = y_transform

    def __getitem__(self, index):
        values = self.data[index]
        target = self.targets[index]
        if self.x_transform is not None:
            values = self.x_transform(values)
        if self.y_transform is not None:
            target = self.y_transform(target)
        return values, target

    def __len__(self):
        return len(self.targets)


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
