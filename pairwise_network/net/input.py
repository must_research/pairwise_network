__author__ = 'jpbeukes27@gmail.com'

import numpy as np


def get_df_combination_idx(combinations, df):
    """Takes a list of input parameter combinations (DataFrame column names)
    and returns the corresponding DataFrame column indices.

    """
    combinations_idx_list = []
    for combination in combinations:
        combinations_idx = []
        for parameter in combination:
            combinations_idx.append(df.columns.get_loc(parameter))
        combinations_idx_list.append(combinations_idx)
    return combinations_idx_list


def get_combinations_from_idx(inputs, combinations_idx):
    combinations = []
    for c in combinations_idx:
        combination = []
        for parameter_idx in c:
            combination.append(inputs[parameter_idx])
        combinations.append(tuple(combination))
    return tuple(combinations)


def get_idx_from_combinations(inputs, combinations):
    combinations_idx = []
    for c in combinations:
        combination = []
        for parameter in c:
            combination.append(inputs.index(parameter))
        combinations_idx.append(tuple(combination))
    return tuple(combinations_idx)


def get_combinations_idx(inputs):
    import itertools
    return tuple(itertools.combinations(np.arange(len(inputs)), 2))


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
