__author__ = 'jpbeukes27@gmail.com'

import os
import copy
import pickle
import logging
import torch

from pairwise_network import net


def get_model_state_dict(model):
    mlp_mask = net.prune_model_remove(model)
    model_state_dict = model.state_dict()
    net.prune_model_apply(model, mlp_mask)
    return copy.deepcopy(model_state_dict)


def load_model_state_dict(model, model_state_dict):
    mlp_mask = net.prune_model_remove(model)
    model.load_state_dict(model_state_dict)
    net.prune_model_apply(model, mlp_mask)


def load_model_state_dict_from_file(model, state_dict_path, use_cuda=False):
    if os.path.isfile(state_dict_path):
        logging.info('Loading model parameters from %s.' % state_dict_path)
        if use_cuda and not torch.cuda.is_available():
            logging.warning('Mapping model to cpu, since cuda not available.')
            load_model_state_dict(model, torch.load(state_dict_path,
                map_location=torch.device('cpu')))
        else:
            load_model_state_dict(model, torch.load(state_dict_path))
    else:
        logging.warning('Model parameters not loaded, because parameter '
                        'file does not exists (%s).', state_dict_path)


def load_model_from_pickle(model_path):
    return pickle.load(open(model_path, 'rb'), encoding='latin1')


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
