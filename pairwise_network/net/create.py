__author__ = 'jpbeukes27@gmail.com'
__description__ = 'Create a pairwise network, and construct the input ' \
                  'combinations.'

import copy
import torch
import torch.nn as nn
from collections import OrderedDict
import logging

from pairwise_network import net


def create_pairwise_model_from_cfg(cfg):
    """Creates a pairwise network."""
    n_timeshift = 0 if cfg['timeshift'] is None else len(cfg['timeshift'])
    model, pairwise_structure, mlp_mask = create_pairwise_model(
        inputs=cfg['inputs'],
        hidden=cfg['hidden'],
        include_phase=True if cfg['phase'] == 'All' else False,
        n_timeshift=n_timeshift,
        add_summary_nodes=cfg['add_summary_nodes'],
        combinations=cfg.get('combinations', []),
        bias_layer=cfg['bias']
    )
    if cfg['device'] == 'cuda':
        if torch.cuda.is_available():
            model = model.cuda()
            mlp_mask_to_device(mlp_mask, 'cuda')
        else:
            logging.warning('Cuda not available. Casting model to cpu.')
    logging.info('Pairwise model created:')
    logging.info(model)
    logging.info('Input combinations: %s'
                 % str(list(pairwise_structure.keys())))
    return model, pairwise_structure, mlp_mask


def create_pairwise_model(inputs, hidden, bias_layer=2, add_summary_nodes=True,
        combinations=(), n_timeshift=0, include_phase=False):
    """Creates a pairwise network by pruning unwanted parameters of a \
     multilayered perceptron (MLP)

    Args:
        combinations(list):
        add_summary_nodes (bool):   Adds an extra hidden layer with a single
                                    node at the output of each subnet
        inputs (list):              input labels
        hidden (list):              number of hidden nodes per hidden layer of
                                    a subnetwork
        include_phase (bool):       phase inputs included when True
        n_timeshift (int):          number of timeshifted inputs to include
        bias_layer (int):           -1 for no bias. Only other option is layer
                                    2 at the moment.

    Returns:
        tuple: (model, pairwise_structure, mlp_mask), where

        model (Sequential): pairwise network as a pruned MLP

        pairwise_structure (dict): indices of pairwise network in the MLP

        mlp_mask (

    """
    hidden_nodes = list(hidden)

    # Input Combinations
    if len(combinations) == 0:
        combinations_idx = net.get_combinations_idx(inputs)
        combinations = net.get_combinations_from_idx(inputs, combinations_idx)
    else:
        combinations_idx = net.get_idx_from_combinations(inputs, combinations)

    # Summary Node
    if add_summary_nodes:
        hidden_nodes.append(1)

    # Dimensions
    n_phases = 3 if include_phase else 0

    n_inputs = (len(inputs) + n_phases) * (n_timeshift + 1)
    n_subnet_input = sum(
        ((len(x) + n_phases) * (n_timeshift + 1) for x in combinations))

    # number of nodes per layer (input included)
    node_num = [n_inputs, n_subnet_input]

    for h in hidden_nodes:
        # number of subnets * number of hidden nodes per layer (the same for
        # all subnets).
        node_num.append(len(combinations) * h)

    # Linear layer 1: groups input combinations.
    layers = OrderedDict()
    grouping_layer = nn.Linear(in_features=node_num[0],
        out_features=node_num[1], bias=False)
    for param in grouping_layer.parameters():
        nn.init.ones_(param)
        # this layer must only pass the input to its group.
        param.requires_grad = False  # no training is required
    layers['lin_1'] = grouping_layer

    # Linear layer 2: subnet input layer (only layer with a bias)
    add_bias = False
    if bias_layer == 2:
        add_bias = True
    layers['lin_2'] = nn.Linear(in_features=node_num[1],
        out_features=node_num[2], bias=add_bias)
    layers['relu_2'] = nn.ReLU()

    # Linear layer 3 to second last layer: subnet hidden layers
    for n in range(2, len(node_num) - 1):
        layers['lin_' + str(n + 1)] = nn.Linear(in_features=node_num[n],
            out_features=node_num[n + 1], bias=False)
        # layers['bn_' + str(n + 1)] = nn.BatchNorm1d(node_num[n + 1])
        layers['relu_' + str(n + 1)] = nn.ReLU()

    # Remove relu from summary nodes
    if add_summary_nodes:
        layers.pop('relu_' + str(len(node_num) - 1))

    # Final linear layer: pairwise output layers (no activation function)
    tag_last_layer = 'lin_' + str(len(node_num))
    layers[tag_last_layer] = nn.Linear(in_features=node_num[-1], out_features=1,
        bias=False)

    model = nn.Sequential(layers)

    # Structure
    pairwise_structure = create_pairwise_structure(
        combinations, combinations_idx, hidden_nodes, len(inputs),
        include_phase, n_timeshift
    )
    mlp_structure = get_mlp_structure_from_pairwise_structure(
        pairwise_structure)

    # Prune
    net.prune_model_apply_from_structure_mlp(model, mlp_structure)
    mlp_mask = net.get_mlp_mask_from_model(model)

    return model, pairwise_structure, mlp_mask


def mlp_mask_to_device(mlp_mask, device):
    for layer in mlp_mask.values():
        for tag, param in layer.items():
            layer[tag] = param.to(device)


def create_pairwise_structure(combinations, combinations_idx, hidden, n_inputs,
        include_phase=False, n_timeshift=0):
    """Generates a dictionary that defines the structure of a pairwise network \
    with respect to the multilayered perceptron (MLP) from which this network \
    will be implemented.

    Args:
        combinations (tuple):       input combinations (labels)
        combinations_idx (tuple):   input combinations (indices)
        hidden (list):              number nodes per hidden layer of a subnet \
                                    (the same for all subnets)
        n_inputs (int):             number of input parameters (without phase \
                                    or timeshifts)

    Returns:
        dict: contains the indices of each subnet and the corresponding \
        indices of the MLP:
            {
            subnet tag : {
                linear layer 0 tag: {
                    'mlp': {'weight': [], 'bias': []},
                    'subnet': {'weight': [], 'bias': []}},
                linear layer 1 tag: {
                    'mlp': {'weight': [], 'bias': []},
                    'subnet': {'weight': [], 'bias': []}},
                ...
                linear layer n tag: {
                    'mlp': {'weight': [], 'bias': []},
                    'subnet': {'weight': [], 'bias': []}}
                }
            }

    """
    n_phases = 3 if include_phase else 0

    # loop through 1) subnets 2) layers
    pairwise_structure = dict()
    subnet_lin2_start = 0
    for s, (combination, combination_idx) in enumerate(
            zip(combinations, combinations_idx)):
        # init pairwise structure
        # -----------------------
        subnet = dict()
        layer_dict = {
            'mlp': {'weight': [], 'bias': []},
            'subnet': {'weight': [], 'bias': []}
        }
        # mlp: weight and bias arrays contain indices of mlp parameters that must not be pruned.
        # subnet: weight and bias arrays contain indices of the subnet if the it where to form its own mlp
        # - format of indices: [out, in]

        # grouping layer
        # --------------
        tag_layer = 'lin_1'
        subnet[tag_layer] = copy.deepcopy(layer_dict)

        lin1_idx_input = list()
        time_group_start = 0
        n_time_group_nodes = n_inputs + n_phases
        for t in range(1, n_timeshift + 2):  # timeshifts
            # inputs
            lin1_idx_input.extend(
                [x + time_group_start for x in combination_idx])
            # phases
            if include_phase:
                phase_start_idx = time_group_start + n_inputs
                lin1_idx_input_phase = list(
                    range(phase_start_idx, phase_start_idx + n_phases))
                lin1_idx_input.extend(lin1_idx_input_phase)

            time_group_start += n_time_group_nodes

        n_lin2_nodes = len(lin1_idx_input)  # TODO: node_num list

        for i in range(n_lin2_nodes):
            mlp_idx = [i + subnet_lin2_start, lin1_idx_input[i]]
            subnet[tag_layer]['mlp']['weight'].append(mlp_idx)
            subnet[tag_layer]['subnet']['weight'].append([i, i])
            # yes, [i, i] is correct. Lin 1 should look like a redundant layer from the subnet's perspective

        # subnet input layer (the only layer with a bias)
        # -----------------------------------------------
        tag_layer = 'lin_2'
        subnet[tag_layer] = copy.deepcopy(layer_dict)
        for i in range(n_lin2_nodes):
            for j in range(hidden[0]):
                mlp_idx = [j + hidden[0] * s, i + subnet_lin2_start]
                subnet[tag_layer]['mlp']['weight'].append(mlp_idx)
                subnet[tag_layer]['subnet']['weight'].append([j, i])

        for j in range(hidden[0]):
            subnet[tag_layer]['mlp']['bias'].append(j + hidden[0] * s)
            subnet[tag_layer]['subnet']['bias'].append(j)

        # subnet hidden layers and output layer
        # -------------------------------------
        for n in range(len(hidden)):
            tag_layer = 'lin_' + str(n + 3)
            subnet[tag_layer] = copy.deepcopy(layer_dict)
            for i in range(hidden[n]):
                in_idx = int(i + hidden[n] * s)
                if n < len(hidden) - 1:
                    for j in range(hidden[n + 1]):
                        out_idx = int(j + hidden[n + 1] * s)
                        mlp_idx = [out_idx, in_idx]
                        subnet[tag_layer]['mlp']['weight'].append(mlp_idx)
                        subnet[tag_layer]['subnet']['weight'].append([j, i])
                else:
                    out_idx = 0
                    subnet[tag_layer]['mlp']['weight'].append([out_idx, in_idx])
                    subnet[tag_layer]['subnet']['weight'].append([out_idx, i])

        # subnet identifier
        # -----------------
        tag_subnet = '-'.join(str(x) for x in combination)
        pairwise_structure[tag_subnet] = subnet
        subnet_lin2_start += n_lin2_nodes
    return pairwise_structure


def get_mlp_structure_from_pairwise_structure(pairwise_structure):
    """Restructure the MLP parameter indices from the pairwise structure \
    dictionary.

    Args:
        pairwise_structure (dict): Expected format:
            {
                subnet tag : {
                    linear layer 0 tag: {
                        'mlp': {'weight': [], 'bias': []},
                        'subnet': {'weight': [], 'bias': []}},
                    linear layer 1 tag: {
                        'mlp': {'weight': [], 'bias': []},
                        'subnet': {'weight': [], 'bias': []}},
                    ...
                    linear layer n tag: {
                        'mlp': {'weight': [], 'bias': []},
                        'subnet': {'weight': [], 'bias': []}}
                    }
            }

    Returns:
        dict: contains the indices for the unmasked pairwise parameters in \
        an MLP:
            {
                linear layer 0 tag: {'weight': [], 'bias': []},
                linear layer 1 tag: {'weight': [], 'bias': []},
                ...
                linear layer n tag: {'weight': [], 'bias': []},
            }

    """
    # get layer tags from first item in structure
    layer_tags = iter(pairwise_structure.values()).__next__().keys()
    # init mlp structure
    structure_mlp = {tag: {'weight': [], 'bias': []} for tag in layer_tags}

    # loop through of every subnet, layer and list of parameters and
    # construct a layer-wise list of parameters
    for tag_subnet, subnet in pairwise_structure.items():
        for tag_layer, layer in subnet.items():
            for tag_param in ('weight', 'bias'):
                idx = layer['mlp'][tag_param]
                structure_mlp[tag_layer][tag_param].extend(idx)

    return structure_mlp


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
