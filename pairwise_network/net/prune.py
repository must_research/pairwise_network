__author__ = 'jpbeukes27@gmail.com'
__description__ = 'Create a pairwise network, and construct the input ' \
                  'combinations.'

import re
import numpy as np
import torch
import torch.nn.utils.prune as prune
import logging

from pairwise_network import net


# -----------------------------------------------------------------------------
# Prune
# -----------------------------------------------------------------------------
def prune_model_apply(model, mlp_mask):
    """Apply pruning to the model using mlp_mask.

        Args:
        mlp_mask (dict): Expected format:
            {
                lin_1: {'weight': (Tensor, optional), \
                    'bias': (Tensor, optional)},
                lin_2: {'weight': (Tensor, optional), \
                    'bias': (Tensor, optional)},
                ...
                lin_n: {'weight': (Tensor, optional), 'bias': (Tensor,
                optional)},
            }

    """
    for tag_layer, module in model.named_children():
        tag_param = 'weight'
        if 'lin' in tag_layer:
            if tag_layer in mlp_mask.keys():
                if tag_param in mlp_mask[tag_layer].keys():
                    logging.debug(f'Pruning layer={tag_layer}, '
                                  f'parameter={tag_param}')
                    mask = mlp_mask[tag_layer][tag_param]
                    prune.custom_from_mask(module, tag_param, mask)


def prune_model_apply_from_structure_mlp(model, structure_mlp):
    for layer_tag, module in model.named_children():
        param_tag = 'weight'
        if 'lin' in layer_tag:
            if layer_tag in structure_mlp.keys():
                if param_tag in structure_mlp[layer_tag].keys():
                    logging.debug('Pruning layer=%s, parameter=%s' % (
                        layer_tag, param_tag))
                    mask = torch.zeros_like(module.weight)
                    for idx in structure_mlp[layer_tag][param_tag]:
                        mask[idx[0], idx[1]] = 1
                    prune.custom_from_mask(module, param_tag, mask)


def prune_model_remove(model):
    """Removes all pairwise parameter buffers and sets the pruned parameter \
    values to zero. Returns the mlp mask that was present before pruning.

    """
    mlp_mask = get_mlp_mask_from_model(model)
    for name, module in model.named_modules():
        if 'lin' in name:
            if 'weight_mask' in dict(module.named_buffers()).keys():
                prune.remove(module, 'weight')
            if 'bias_mask' in dict(module.named_buffers()).keys():
                prune.remove(module, 'bias')
    return mlp_mask


def get_mlp_mask_from_model(model):
    mlp_mask = {}
    for name, module in model.named_modules():
        if 'lin' in name:
            if 'weight_mask' in dict(module.named_buffers()).keys():
                mlp_mask[name] = {'weight': module.weight_mask}
            if 'bias_mask' in dict(module.named_buffers()).keys():
                mlp_mask[name] = {'bias': module.bias_mask}
    return mlp_mask


def prune_per_layer(model, amount, prune_type='l1_unstructured'):
    """Prune the model per layer by the given amount.

    Args:
        model (Sequentail): pairwise network model
        amount (float):     percentage of remaining weights to remove (0 to 1)
        prune_type (str):   only `l1_unstructured` is implemented

    """
    n_layers = net.get_num_layers_from_model(model)
    for tag, module in model.named_modules():
        match = re.match(r'^lin_([\d]+)$', tag)
        if match:
            layer = int(match.group(1))
            if layer not in [1, n_layers]:
                tag_param = 'weight'
                if prune_type == 'l1_unstructured':
                    logging.info(f'Pruning layer={tag}, parameter={tag_param}, '
                                 f'L1 Unstructured')
                    prune.l1_unstructured(module, tag_param, amount)


def get_model_sparsity(model, pairwise_structure):
    mlp_mask = net.prune_model_remove(model)

    w = net.get_pairwise_weights(model, pairwise_structure)
    sparsity_subnet = {}
    sparsity_layers = w
    n_params_global = 0
    n_pruned_global = 0
    for tag_subnet, subnet in w.items():
        n_params_subnet = 0
        n_pruned_subnet = 0
        for tag_layer, layer in subnet.items():
            n_params = float(np.size(layer))
            n_pruned = float(np.sum(layer == 0))
            sparsity_layers[tag_subnet][tag_layer] = n_pruned / n_params

            n_params_subnet += n_params
            n_pruned_subnet += n_pruned

        n_params_global += n_params_subnet
        n_pruned_global += n_pruned_subnet
        sparsity_subnet[tag_subnet] = n_pruned_subnet / n_params_subnet
        logging.debug('Global sparsity of subnet %s: %.4f' % (tag_subnet,
        sparsity_subnet[tag_subnet]))

    sparsity_global = n_pruned_global / n_params_global
    net.prune_model_apply(model, mlp_mask)
    return sparsity_global, sparsity_subnet, sparsity_layers


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
