__author__ = 'jpbeukes27@gmail.com'

import re


def get_num_layers_from_cfg(cfg):
    n_layers = 2  # Pairwise input + subnet input
    n_layers += len(cfg['hidden'])
    if cfg['add_summary_nodes']:
        n_layers += 1
    return n_layers


def get_num_layers_from_model(model):
    n_layers = 0  # Pairwise input + subnet input
    for tag_layer, _ in model.named_children():
        if re.match(r'^lin_([\d]+)$', tag_layer):
            n_layers += 1
    return n_layers


def parse_layer_and_param_tag(tag):
    match = re.match(r'^lin_([\d]+)\.?(\w+)?$', tag)
    if match:
        layer = match.group(1)
        param_type = match.group(2)
        return True, param_type, int(layer)
    else:
        return False, '', 0


def parse_layer_tag(tag):
    match = re.match(r'^lin_([\d]+)$', tag)
    if match:
        layer = match.group(1)
        return True, int(layer)
    else:
        return False, 0


def input_dim(model):
    """Returns input dimension of the model."""
    return model.lin_1.in_features


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
