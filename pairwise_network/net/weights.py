__author__ = 'jpbeukes27@gmail.com'

import logging
import re
import numpy as np
import pandas as pd
import torch

from pairwise_network import net


# -----------------------------------------------------------------------------
# Pairwise weights
# -----------------------------------------------------------------------------
def get_pairwise_weights(model, pairwise_structure):
    """Extracts the weights of a pairwise model from the corresponding MLP \
    using the provided structure indices.

    Args:
        model (Sequential): pairwise model (pruned MLP)
        pairwise_structure (dict): indices of the pairwise model within the MLP

    Returns:
        dict: a dictionary of subnets dictionaries. Each subnet is a \
        dictionary of arrays representing the weight matrices of each linear \
        layer in the model. It has the following format:
            {
                subnet tag : {
                    1: ndarray,
                    2: ndarray,
                    ...
                    n: ndarray,
                }
            }

    """
    mlp_mask = net.prune_model_remove(model)
    w = {}  # weights of the entire model

    # loop through subnet structures in  pairwise structure
    for tag_subnet, structure_subnet in pairwise_structure.items():
        w_subnet = {}

        # loop through layer structures in subnet structure
        for tag_layer, structure_layer in structure_subnet.items():
            is_linear, _, layer = net.parse_layer_and_param_tag(tag_layer)
            if is_linear:

                # get subnet- and corresponding mlp weight indices
                idx_subnet = np.array(structure_layer['subnet']['weight'])
                idx_mlp = np.array(structure_layer['mlp']['weight'])

                # get number of input and output features of the subnet layer
                out_features = np.unique(idx_subnet[:, 0]).size
                in_features = np.unique(idx_subnet[:, 1]).size

                # init subnet layer weight matix
                w_layer = np.zeros(shape=(out_features, in_features),
                    dtype=float)

                # loop through modules in mlp
                for name, mlp_module in list(model.named_modules()):

                    # find the layer that matches the subnet layer
                    if tag_layer == name:

                        # extract the weight values according to the indices
                        # in the subnet layer structure
                        w_mlp_module = mlp_module.weight.data.detach().cpu().numpy()
                        for i in range(idx_subnet.shape[0]):
                            w_layer[idx_subnet[i, 0], idx_subnet[i, 1]] = \
                                w_mlp_module[idx_mlp[i, 0], idx_mlp[i, 1]]

                        # extract biases if there are any
                        if mlp_module.bias is not None:
                            b_mlp_module = mlp_module.bias.data.detach().cpu().numpy()
                            idx_subnet = np.array(
                                structure_layer['subnet']['bias'])
                            idx_mlp = np.array(structure_layer['mlp']['bias'])
                            features = np.unique(idx_subnet).size
                            b_layer = np.zeros(shape=features, dtype=float)
                            for i in range(idx_subnet.size):
                                b_layer[idx_subnet[i]] = b_mlp_module[
                                    idx_mlp[i]]

                            # concat the bias values to the layer's weight array
                            w_layer = np.concatenate(
                                (w_layer, b_layer.reshape(-1, 1)), axis=1)

                w_subnet[layer] = w_layer
        w[tag_subnet] = w_subnet
    net.prune_model_apply(model, mlp_mask)
    return w


def set_pairwise_weights(model, pairwise_structure, w):
    """Replace the weights and biases of a pairwise model with w.

    Args:
        model (Sequential): pairwise model (pruned MLP)
        pairwise_structure (dict): indices of the pairwise model within the MLP
        w (dict): a dictionary of subnets dictionaries. Each subnet is a \
            dictionary of arrays representing the weight matrices of each \
            linear layer in the model.

    """
    # loop through subnet structures in  pairwise structure
    for tag_subnet, structure_subnet in pairwise_structure.items():
        w_subnet = w[tag_subnet]

        # loop through layer structures in subnet structure
        for tag_layer, structure_layer in structure_subnet.items():
            is_linear, _, layer = net.parse_layer_and_param_tag(tag_layer)
            if is_linear:

                w_layer = w_subnet[layer]

                # get subnet- and corresponding mlp weight indices
                subnet_weight_idx = np.array(
                    structure_layer['subnet']['weight'])
                mlp_weight_idx = np.array(structure_layer['mlp']['weight'])

                # loop through modules in mlp
                for name, mlp_module in list(model.named_modules()):

                    # find the layer that matches the subnet layer
                    if tag_layer == name:

                        param_tags = list(
                            dict(mlp_module.named_parameters()).keys())

                        # extract biases if there are any
                        if 'bias' in param_tags or 'bias_orig' in param_tags:
                            subnet_bias_idx = np.array(
                                structure_layer['subnet']['bias'])
                            mlp_bias_idx = np.array(
                                structure_layer['mlp']['bias'])
                            # extract biases
                            b_layer = torch.tensor(w_layer[:, -1])
                            # remove biases
                            w_layer = w_layer[:, :-1]

                            for i in range(subnet_bias_idx.size):
                                b_mlp = b_layer[subnet_bias_idx[i]]
                                mlp_module.bias.data[mlp_bias_idx[i]] = b_mlp
                                # set both the current and original weights
                                if 'bias_orig' in param_tags:
                                    mlp_module.bias_orig.data[
                                        mlp_bias_idx[i]] = b_mlp

                        # extract the weight values according to the indices in the subnet layer structure
                        if 'weight' in param_tags or 'weight_orig' in param_tags:
                            for i in range(subnet_weight_idx.shape[0]):
                                w_mlp = w_layer[
                                    subnet_weight_idx[i, 0], subnet_weight_idx[
                                        i, 1]]
                                mlp_module.weight.data[
                                    mlp_weight_idx[i, 0], mlp_weight_idx[
                                        i, 1]] = w_mlp
                                if 'weight_orig' in param_tags:
                                    mlp_module.weight_orig.data[
                                        mlp_weight_idx[i, 0], mlp_weight_idx[
                                            i, 1]] = w_mlp


def normalise_pairwise_weights(w, layer_start=1):
    """Normalise the weights of each subnet in a pairwise model

    Args:
        layer_start (int):  layer at which to start normalizing
        w (dict):           weights of a subnet:
            {
                subnet tag : {
                    1: ndarray,
                    2: ndarray,
                    ...
                    n: ndarray,
                }
            }

    """
    for i, (c, w_subnet) in enumerate(w.items()):
        # c: combination pair (ie. subnet id), w_subnet: subnet weights
        for l in range(layer_start, len(w_subnet)):
            # for number of layers in subnet
            for n in range(0, w_subnet[l].shape[0]):
                # for number of nodes in layer

                # get norm of the fan-in weights of node n in layer l
                # (Note: the biases in the first layer of the pairwise biases
                # are included here)
                val = np.linalg.norm(w_subnet[l][n, :])

                # divide the fan-in weights of node n in layer l by its norm
                w_subnet[l][n, :] = np.divide(w_subnet[l][n, :], val)

                # multiply the fan-out weights of node n in layer l by the
                # norm of the fan-in weights.
                w_subnet[l + 1][:, n] = np.multiply(w_subnet[l + 1][:, n], val)

                w[c] = w_subnet


# -----------------------------------------------------------------------------
# MLP weights
# -----------------------------------------------------------------------------
def get_mlp_weights(model):
    """Get MLP weights.

    References:
        mustnet3

    Returns:
        dict: MLP weights {layer number (int): weights (ndarray)}

    """
    mlp_mask = net.prune_model_remove(model)

    def parse_tag(tag):
        match = re.match(r'^lin_([\dN]+)\.(\w+)$', tag)
        if match:
            layer = match.group(1)
            param_type = match.group(2)
            return True, param_type, int(layer)
        else:
            return False, '', 0

    W = {}
    # first run through weights, in case needed when adding a bias
    for name, param in model.named_parameters():
        is_linear, param_type, layer = parse_tag(name)
        if is_linear:
            if param_type == 'weight':
                W[layer] = param.cpu().data.numpy()
    for name, param in model.named_parameters():
        is_linear, ptype, layer = parse_tag(name)
        if is_linear:
            if ptype == 'bias':
                W[layer] = np.c_[W[layer], param.cpu().data.numpy()]

    n_layers = net.get_num_layers_from_model(model)

    for i in range(1, n_layers + 1):
        logging.debug('Weight matrix at layer %d: %d x %d', i,
            W[i].shape[0], W[i].shape[1])
    net.prune_model_apply(model, mlp_mask)
    return W


# -----------------------------------------------------------------------------
# Layer 2
# -----------------------------------------------------------------------------
def get_subnet_lin_2_tags(subnet_structure, input_tags, include_bias=True):
    """Extract the parameter tags of the subnet's input nodes (layer 2).
    Achieved by mapping the input tags of input nodes in the first layer of a \
    pairwise model to the nodes in layer 2 according to the indices of the \
    given subnet.

    Args:
        subnet_structure (dict):    subnet indices, taken from the pairwise
                                    structure.
        input_tags (list):          input labels of the pairwise model. The
                                    order must correspond to the pairwise
                                    network's input nodes.
        include_bias (bool):        True if bias should be included. False
                                    otherwise.

    Returns:
        list: Subnet input tags.

    """
    mlp_lin_1_idx = np.array(subnet_structure['lin_1']['mlp']['weight'])
    # Get the indices of the inputs connected to this subnet's layer 2
    input_idx = mlp_lin_1_idx[:, 1]
    # Map index to tag
    result = [input_tags[i] for i in input_idx]

    if include_bias:
        result.append('bias')

    return result


def rename_subnets_based_on_lin_2(model, pairwise_structure, input_tags,
        precision=9, tag_dead=True, add_bias=False):
    """Rename subnets according to the weight values in layer 2. If all the
    fan-out weights of a node is equal to zero, then this input is removed
    from the subnet tag. If both nodes are removed, the subnet is named
    `bias` and if there are no living biases, the subnet is named `dead`.

    Args:
        model (Sequential):         pairwise network.
        pairwise_structure (dict):  pairwise structure.
        input_tags (list):          input parameter labels
        precision (int):            number of decimals to round fan-out
                                    weight values.
        tag_dead (bool):            if True, nodes with all-zero fan-out
                                    weights and biases are tagged `dead`.
        add_bias (bool):        if True, `bias` tags are added to the
                                    renamed subnet tag.

    Returns:
        list: Renamed subnet tags.

    """
    w_pairwise = net.get_pairwise_weights(model, pairwise_structure)

    renamed_input_tags = list()
    for subnet_tag, w_subnet in w_pairwise.items():  # Loop over subnets
        # Extract original subnet input node (layer 2) tags
        layer_tags = net.get_subnet_lin_2_tags(pairwise_structure[subnet_tag],
            input_tags, include_bias=True)

        # Get subnet layer 2 fan-out weights
        w_layer = w_subnet[2]
        tags = []
        for i in range(w_layer.shape[1]):  # Loop over layer nodes
            node_tag = layer_tags[i]
            w_node = w_layer[:, i]  # Fan-out weights of node
            if not all(w_node.round(precision) == 0):
                tags.append(node_tag)

        renamed_input_tags.append(tags)

    # Remove bias from tags unless it is the only item in the list
    for i in range(len(renamed_input_tags)):
        tags = renamed_input_tags[i]

        if tags != ['bias']:
            if not add_bias:
                if 'bias' in tags:
                    tags.remove('bias')

        if tag_dead:
            if len(tags) == 0:
                renamed_input_tags[i] = ['dead']

    renamed_subnet_tags = ['-'.join(tag) for tag in renamed_input_tags]

    return renamed_subnet_tags


# -----------------------------------------------------------------------------
# Summary Nodes
# -----------------------------------------------------------------------------
def get_sn_weights(model):
    n_layers = net.get_num_layers_from_model(model)
    w_mlp = net.get_mlp_weights(model)
    w_sn = w_mlp[n_layers]
    return w_sn


def get_normalized_sn_weights(model, pairwise_structure):
    """

    Args:
        model:
        pairwise_structure:

    Returns:
        ndarray: Matrix of summary node weights.

    """
    w = get_pairwise_weights(model, pairwise_structure)
    normalise_pairwise_weights(w)
    n_layers = net.get_num_layers_from_model(model)
    w_sn = []
    for w_subnet in w.values():
        w_sn.append(float(w_subnet[n_layers]))
    return np.array(w_sn).reshape((1, -1))


def get_sn_weights_df(model, pairwise_structure):
    w = get_sn_weights(model).squeeze()
    w_normalized = get_normalized_sn_weights(model,
        pairwise_structure).squeeze()
    result = {
        'combinations': list(pairwise_structure.keys()),
        'sn': w,
        'sn_abs': np.abs(w),
        'sn_normalized': w_normalized,
        'sn_normalized_abs': np.abs(w_normalized)
    }
    df = pd.DataFrame(result)
    return df


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
