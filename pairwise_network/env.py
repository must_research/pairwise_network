__author__ = 'jpbeukes27@gmail.com'

import os
import logging

import numpy as np
import torch

# -----------------------------------------------------------------------------
# Paths: General
# -----------------------------------------------------------------------------
repo_dir = os.path.expanduser('~/repos/pairwise_network')
"""str: directory where pairwise repository are kept."""

data_dir = os.path.expanduser('~/datasets')
"""str: root of directory where data kept."""

data_dir_symh = os.path.expanduser(os.path.join(data_dir, 'symh_v2'))
"""str: root of directory where symhv2 data kept."""

data_dir_art = os.path.expanduser(os.path.join(data_dir, 'art'))
"""str: root of directory where artificial datasets are kept."""

source_root_dir = os.path.join(repo_dir, 'pairwise_network')
"""str: directory where pairwise source code is kept."""

config_dir = os.path.join(repo_dir, 'config')
"""str: directory where configs are kept."""

project_dir = os.path.expanduser('~/projects/pairwise/')
"""str: root directory where all outputs related to training and analysis 
created."""

results_dir = os.path.join(project_dir, 'results')
"""str: directory where analysis results are kept."""

test_dir = os.path.join(repo_dir, 'test')
"""str: PyTest directory."""

test_config_dir = os.path.join(test_dir, 'config')
"""str: PyTest config directory."""


def create_dir_if_not_exist(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)


# -----------------------------------------------------------------------------
# Paths: Create
# -----------------------------------------------------------------------------
for dir in (project_dir, results_dir):
    create_dir_if_not_exist(dir)

# -----------------------------------------------------------------------------
# Paths: Output
# -----------------------------------------------------------------------------
out_dir = os.path.join(project_dir, 'out')
"""str: directory where train and test output are kept."""

create_dir_if_not_exist(out_dir)


def get_run_out_dir(run_id):
    """Get path to local run output directory and creates it if not exists."""
    run_dir = os.path.join(out_dir, run_id)
    if not os.path.exists(run_dir):
        os.makedirs(run_dir)
    return run_dir


# -----------------------------------------------------------------------------
# Paths: Analyse
# -----------------------------------------------------------------------------
analyse_dir = os.path.join(project_dir, 'analyse')
"""str: directory where analysis output are kept."""

create_dir_if_not_exist(analyse_dir)


def get_run_analyse_dir(run_id):
    """Get path to local run analyse directory and creates it if not exists."""
    run_dir = os.path.join(analyse_dir, run_id)
    if not os.path.exists(run_dir):
        os.makedirs(run_dir)
    return run_dir


def get_run_sn_act_path(run_id, partition):
    """Get file path of the unweighted summary node activation values for a
    given run."""
    run_analyse_dir = get_run_analyse_dir(run_id)
    return os.path.join(run_analyse_dir, f'sn-act-{partition}.csv')


# -----------------------------------------------------------------------------
# Default Filenames
# -----------------------------------------------------------------------------
model_filename = 'model.pt'
pairwise_structure_filename = 'pairwise_structure.pkl'
mlp_mask_filename = 'mlp_mask.pkl'
act_dist_filename = 'act_dist.pkl'
prune_save_filename = 'prune_save.pkl'

cfg_default_filename = 'config-symh.yaml'

art_meta_filename = 'meta.yaml'

# -----------------------------------------------------------------------------
# Weights & Biases
# -----------------------------------------------------------------------------
wandb_project = 'pairwise'
wandb_entity = 'jpbeukes27'
wandb_project_path = '/'.join((wandb_entity, wandb_project))

os.environ['WANDB_PROJECT'] = wandb_project
os.environ['WANDB_USERNAME'] = wandb_entity
os.environ['USE_WANDB'] = str(False)
os.environ['WANDB_DIR'] = project_dir


def enable_wandb():
    os.environ['USE_WANDB'] = str(True)


def wandb_is_online():
    return os.environ.get('USE_WANDB', str(False)) == str(True)


# -----------------------------------------------------------------------------
# Threads, Seeds, Cuda and Logging
# -----------------------------------------------------------------------------
max_num_threads = 4
"""int: max number of cpu threads during training."""

num_dataloader_workers = 12
"""int: number of subprocesses to use for data loading."""


def set_seeds(seed):
    np.random.seed(seed)
    torch.manual_seed(seed)


def set_log_level(log_level='INFO'):
    logging.basicConfig(level=log_level,
        format='%(asctime)s - %(levelname)s - %(message)s')


def set_device(device):
    if device == 'cuda':
        if torch.cuda.is_available():
            logging.info('Device: cuda')
        else:
            device = 'cpu'
            logging.info('Device: cpu (cuda not available)')
    else:
        logging.info('Device: cpu')
    return device


# -----------------------------------------------------------------------------
# Common constants
# -----------------------------------------------------------------------------
partitions = ('train', 'valid', 'eval')
