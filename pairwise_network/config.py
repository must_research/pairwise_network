__author__ = 'jpbeukes27@gmail.com'

import logging
from sys import exit

import yaml

from pairwise_network import env

cfg_default = {
    'device': 'cpu',
    'dataset': 'test',
    'art_function': 'x1 * x2',
    'art_n_samples': -1,
    'art_split_valid_perc': 20,
    'art_split_eval_perc': 0,
    'art_split_method': 'sequential',
    'art_save_load_locally': False,
    'architecture': 'pairwise',
    'inputs': ['x1', 'x2', 'x3'],
    'hidden': [10, 10],
    'bias': 2,
    'activation': 'relu',
    'outputs': ['y'],
    'add_summary_nodes': True,
    'init_scheme': 'kaiming',
    'max_epochs': 100,
    'max_epoch_patience': -1,
    'tolerance': 1e-05,
    'patience': 100,
    'seed': 1,
    'batch_size': 64,
    'optimizer': 'adam',
    'lr': 0.0001,
    'adam_beta1': 0.9,
    'adam_beta2': 0.999,
    'weight_decay': 0,
    'loss': 'mse',
    'es_enabled': True,
    'es_check_perc': 0,
    'es_step_perc': 0,
    'standardize_dataset': True,
    'phase': None,
    'prune_amount': 0.15,
    'prune_type': 'l1_unstructured',
    'prune_iterations_max': 0,
    'prune_tolerance': 0.05,
    'prune_epochs': 50,
    'prune_init_method': 'continue',
    'art_standardize': False,
    'timeshift': 'None',
    'prune_constant_epochs': False,
}


def cfg_from_yaml(cfg_filename=None):
    """Extracts config information from the given yaml file and returns as a \
    ConfigDict.

    """
    if cfg_filename is None:
        cfg_filename = env.cfg_default_filename
    f = open(cfg_filename, 'r')

    cfg_yaml = None

    try:
        cfg_yaml = yaml.full_load(f)
    except Exception as e:
        logging.error('Error loading config %s:\n%s' % (cfg_filename, str(e)))
        exit(101)
    finally:
        f.close()

    cfg = ConfigDict()
    for key in cfg_yaml.keys():
        cfg[key] = cfg_yaml[key]['value']

    return cfg


def update_cfg_with_wandb_cfg(cfg, wandb_cfg):
    updated_cfg = ConfigDict()
    updated_cfg.update(cfg)
    for key in dict(wandb_cfg).keys():
        updated_cfg[key] = dict(wandb_cfg)[key]
    return updated_cfg


class ConfigDict(dict):
    """Adds missing key handling with default values and wandb updates \
    to the builtin dict.

    """

    def __init__(self):
        super().__init__()

    def preprocess_get(self, key, value):
        if str(value).upper() == 'NONE':
            value = None
        return value

    def __getitem__(self, key):
        # If key not in dict, assign default value
        if key not in self.keys():
            if key not in cfg_default.keys():
                logging.error(f'"{key}" not found in config or default config.')
                exit(101)
            value = cfg_default[key]
            logging.warning(f'"{key}" not found in config. '
                            f'Using default: {value}')
            return self.preprocess_get(key, value)

        value = super(ConfigDict, self).__getitem__(key)
        return self.preprocess_get(key, value)

    def __setitem__(self, key, value):
        # Update wandb config if online
        if env.wandb_is_online():
            import wandb
            wandb.config.update({key: value}, allow_val_change=True)
        super(ConfigDict, self).__setitem__(key, value)
