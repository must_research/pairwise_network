__author__ = 'jpbeukes27@gmail.com'

import pandas as pd
import numpy as np
from sys import exit

import sympy

import logging


# -----------------------------------------------------------------------------
# Split
# -----------------------------------------------------------------------------
def split_df(df, split_method='shuffle', valid_perc=0, eval_perc=0):
    """Split DataFrame into partitions according to the given ratios. If \
    split percentages is zero, the DataFrame is replaced with None.

    Args:
        df (DataFrame):     entire dataset.
        split_method (str): options: shuffle, sequential.
        valid_perc (int):   % of entire dataset allocated to validation set.
        eval_perc (int):    % of entire dataset allocated to evaluation set.

    Returns:
        list: DataFrames: (train, valid, eval). None if split percentage is 0.

    """
    logging.info('Splitting DataFrame. Method: %s' % split_method)
    n_total = df.shape[0]
    n_valid = int(n_total * valid_perc / 100)
    n_eval = int(n_total * eval_perc / 100)
    n_train = int(n_total - n_eval - n_valid)

    train_index = valid_index = eval_index = []
    if split_method == 'sequential':
        train_index = list(range(0, n_train))
        valid_index = list(range(n_train, n_train + n_valid))
        eval_index = list(range(n_train + n_valid, n_total))
    elif split_method == 'shuffle':
        df_index = np.random.permutation(list(df.index))
        train_index = df_index[0: n_train]
        valid_index = df_index[n_train: n_train + n_valid]
        eval_index = df_index[n_train + n_valid: n_total]
    else:
        logging.error('Undefined splitting method: %s' % split_method)
        exit(101)

    df_datasets = [df.loc[train_index], ]

    if valid_perc != 0:
        df_datasets.append(df.loc[valid_index])
    else:
        logging.info(f'No validation set, since split percentage={valid_perc}')
        df_datasets.append(None)

    if eval_perc != 0:
        df_datasets.append(df.loc[eval_index])
    else:
        logging.info(f'No evaluation set, since split percentage={eval_perc}')
        df_datasets.append(None)

    partitions = ('train', 'valid', 'eval')
    for i in range(len(partitions)):
        df = df_datasets[i]
        p = partitions[i]
        if df is not None:
            logging.info(f'Number of samples in {p} set: {df.shape[0]}')

    return df_datasets


# -----------------------------------------------------------------------------
# Sympy
# -----------------------------------------------------------------------------
def sympy_function_to_targets(sympy_function, data):
    """Applies a function on the data.

    Args:
        sympy_function (str): Sympy-compatible function.
        data (ndarray): Input data matrix.

    Returns:
        ndarray: Vector of target values.

    """
    expression = sympy.sympify(sympy_function)
    symbols = list(expression.atoms(sympy.Symbol))

    n_symbols = len(symbols)
    n_samples = data.shape[0]
    n_features = data.shape[1]

    if n_features < n_symbols:
        logging.error('Number of input features is less than the'
                      'number of variables in the symbolic function.')
        exit(101)

    targets = np.zeros(n_samples)
    for i in range(0, n_samples):
        sub = []
        for k in range(0, n_symbols):
            sub.append((symbols[k], data[i, k]))
        targets[i] = expression.subs(sub)

    return targets


def calc_target_plane(sympy_func, x1, x2):
    y_true = np.zeros((len(x1), len(x2)), dtype=float)
    data = np.zeros((len(x1), 2), dtype=float)
    data[:, 0] = x1
    for i in range(len(x2)):
        data[:, 1] = np.full_like(x1, x2[i])
        y_true[:, i] = sympy_function_to_targets(sympy_func, data)
    return y_true


# -----------------------------------------------------------------------------
# Create artificial data
# -----------------------------------------------------------------------------
def create_snd(sympy_func, input_tags, output_tag, n_samples=500,
        output_noise=None):
    """Create a dataset where the target is a function of a subset of data \
    drawn from a standard normal distribution:

    y = f(Z), where Z is a subset of X ~ N(0, 1).

    Args:
        sympy_func (str):   Sympy-compatible function.
        input_tags (list):  Input parameter labels.
        output_tag (str):   Target parameter label.
        n_samples (int):    Number of samples
        output_noise (None, Tuple): Add noise to the target variable. Expects a
            tuple: (mean, standard deviation). If not provided, no noise is
            added.

    Returns:
        DataFrame: Data and targets in a single dataframe.

    """
    n_inputs = len(input_tags)
    data = np.random.normal(0, 1, (n_samples, n_inputs))

    y = sympy_function_to_targets(sympy_func, data)
    if output_noise is not None:
        y += np.random.normal(output_noise[0], output_noise[1], y.shape)

    column_tags = list(input_tags)
    column_tags.append(output_tag)
    df = pd.DataFrame(
        data=np.concatenate([data, y.reshape(-1, 1)], axis=1),
        columns=column_tags
    )

    return df


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
