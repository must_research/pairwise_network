__author__ = 'jpbeukes27@gmail.com'

import logging

from pairwise_network.art.art_files import dataset_exists, save_dataset, \
    load_dataset
from pairwise_network.art import art_core
from pairwise_network import env


def create_art_dataset(cfg, reload=False, data_dir=None):
    dataset_tag = cfg['dataset']

    if dataset_tag == 'art-snd':
        df_datasets = snd(
            output_function=cfg['art_function'],
            input_tags=cfg['inputs'],
            output_tag=cfg['outputs'][0],
            n_samples=cfg['art_n_samples'],
            output_noise=None,
            split_method=cfg['art_split_method'],
            valid_perc=cfg['art_split_valid_perc'],
            eval_perc=cfg['art_split_eval_perc'],
            seed=1,
            reload=reload,
            data_dir=data_dir
        )
        env.set_seeds(cfg['seed'])
        return df_datasets
    else:
        logging.error(f'Dataset {dataset_tag} not implemented.')
        exit(101)


def snd(output_function, input_tags, output_tag, n_samples=500,
        output_noise=None, split_method='shuffle', valid_perc=0, eval_perc=0,
        seed=1, reload=False, data_dir=None):
    dataset_tag = 'art-snd'

    meta_data = dict(
        dataset_tag=dataset_tag,
        output_function=output_function,
        input_tags=input_tags,
        output_tag=output_tag,
        n_samples=n_samples,
        output_noise=output_noise,
        split_method=split_method,
        valid_perc=valid_perc,
        eval_perc=eval_perc,
        seed=seed,
    )

    if not dataset_exists(dataset_tag, meta_data, data_dir) or reload:
        env.set_seeds(seed)
        logging.info(f'Generating new dataset: {dataset_tag}')
        df = art_core.create_snd(output_function, input_tags, output_tag,
            n_samples, output_noise)

        df_datasets = art_core.split_df(df, split_method, valid_perc, eval_perc)
        save_dataset(dataset_tag, df_datasets, meta_data, reload, data_dir)
    else:
        df_datasets = load_dataset(dataset_tag, meta_data, data_dir)

    return df_datasets


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
