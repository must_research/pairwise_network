__author__ = 'jpbeukes27@gmail.com'

import logging
import os
import re

import pandas as pd
import yaml

from pairwise_network import env


def get_dataset_dir(dataset_tag, data_dir=None):
    """Returns the dataset directory of the given dataset name."""
    if data_dir is None:
        data_dir = env.data_dir
    else:
        data_dir = os.path.expanduser(data_dir)
    dataset_dir = os.path.join(data_dir, dataset_tag)
    return dataset_dir


def get_dataset_versions(dataset_dir):
    """Returns a list of all the existing versions of the provided dataset."""
    subdirs = os.listdir(dataset_dir)
    versions = list()
    for dir in subdirs:
        match = re.match(r'v(\d+)$', dir)
        if match:
            versions.append(int(match.groups()[0]))
    return versions


def get_version_dir(dataset_dir, version):
    """Returns the directory to the dataset version."""
    version_dir = os.path.join(dataset_dir, 'v' + str(version))
    return version_dir


def meta_in_version(dataset_dir, version, meta_data):
    """Returns True if the dataset version has the given meta data. False \
    otherwise.

    """
    version_dir = get_version_dir(dataset_dir, version)
    meta_path = os.path.join(version_dir, env.art_meta_filename)
    if os.path.exists(meta_path):
        f = open(meta_path, 'r')
        meta_file = yaml.full_load(f)
        if meta_data == meta_file:
            return True
    return False


def find_version(dataset_dir, meta_data):
    """Returns the first dataset version number that matches the meta data. \
    Returns None if not found.

    """
    versions = get_dataset_versions(dataset_dir)

    for v in versions:
        if meta_in_version(dataset_dir, v, meta_data):
            return v

    return None


def find_version_dir(dataset_dir, meta_data):
    """Wrapper for `find_version`. Returns the directory of the dataset \
    version matching meta data. Returns None if no version is found.

    """
    version = find_version(dataset_dir, meta_data)
    if version is None:
        return None
    else:
        version_dir = get_version_dir(dataset_dir, version)
        return version_dir


def dataset_exists(dataset_tag, meta_data, data_dir=None):
    """Wrapper for `find_version`. True if dataset exists, False otherwise. \
    Creates dataset path if not exists.
    """
    dataset_dir = get_dataset_dir(dataset_tag, data_dir)
    if os.path.exists(dataset_dir):
        version = find_version(dataset_dir, meta_data)
        # TODO: Check if partitions exist
        return version is not None
    else:
        os.makedirs(dataset_dir)


def find_partition_path(dataset_tag, meta_data, partition_tag, data_dir=None):
    """Returns the path of a partition of a dataset version that matches the
    given meta data.

    """
    dataset_dir = get_dataset_dir(dataset_tag, data_dir)
    version_dir = find_version_dir(dataset_dir, meta_data)
    partition_path = os.path.join(version_dir, partition_tag + '.pkl')
    return partition_path


def max_version(dataset_dir):
    """Returns the largest version number and zero if none exist."""
    versions = get_dataset_versions(dataset_dir)
    if len(versions) == 0:
        return 0
    else:
        return max(versions)


def save_dataset(dataset_tag, df_datasets, meta_data, unsafe=False,
                 data_dir=None):
    """Save all the partitions of a dataset and its meta data to a directory
    with a specific version.

    Args:
        dataset_tag (str):  dataset name
        df_datasets (list): partitions (train, valid, eval). (DataFrames)
        meta_data (dict):   meta data
        unsafe (bool):      if True, existing directories will be
            overwritten. Otherwise, the program will exit.

    """
    dataset_dir = get_dataset_dir(dataset_tag, data_dir)
    version = find_version(dataset_dir, meta_data)

    if version is None:
        version = max_version(dataset_dir) + 1
    elif not unsafe:
        version_dir = get_version_dir(dataset_dir, version)
        logging.error('Remove existing version first: ' + version_dir)
        exit(101)

    version_dir = get_version_dir(dataset_dir, version)
    env.create_dir_if_not_exist(version_dir)

    # Save meta data
    meta_path = os.path.join(version_dir, env.art_meta_filename)
    f = open(meta_path, 'w')
    meta_yaml = yaml.dump(meta_data, sort_keys=False)
    f.write(meta_yaml)
    f.close()

    # Save partitions
    for p, df in zip(env.partitions, df_datasets):
        file_path = find_partition_path(dataset_tag, meta_data, p, data_dir)
        if df is not None:
            df.to_pickle(file_path)
            logging.info(f'Partition {p} saved to {file_path}.')


def load_dataset(dataset_tag, meta_data, data_dir=None):
    """Load the partitions that corresponds to the dataset tag and meta data. \
    Exits if matching dataset versions are found.

    Args:
        dataset_tag (str):  dataset name
        meta_data (dict):   meta data

    Returns:
        list: a DataFrame for each partition: (train, valid, test). None, if
            the particular partition are not found.

    """
    dataset_dir = get_dataset_dir(dataset_tag, data_dir)
    version_dir = find_version_dir(dataset_dir, meta_data)
    if version_dir is None:
        logging.error(f'No version of dataset {dataset_tag} found.')
        exit(101)
    else:
        df_datasets = list()
        for p in env.partitions:
            file_path = find_partition_path(dataset_tag, meta_data, p, data_dir)
            if os.path.exists(file_path):
                logging.info(f'Loading partition {p} from {file_path}')
                df_datasets.append(pd.read_pickle(file_path))
            else:
                logging.info(f'No {p} partition found.')
                df_datasets.append(None)

        return df_datasets


# -----------------------------------------------------------------------------
if __name__ == '__main__':
    pass
# -----------------------------------------------------------------------------
