#!/bin/bash

for y in 'x1*x2'
do
  for lr in 0.0001
  do
    for seed in 1 2 3 4
    do
      echo "Training with seed $seed."
      pw-build train ../config/art-snd.yaml \
      -n "seed=$seed" \
      -s $seed \
      -l $lr \
      -y "$y" \
      --wandb&
    done
  done
done
