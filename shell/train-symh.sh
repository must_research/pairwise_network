#!/bin/bash

for seed in 1 2 3
do
  echo "Training with seed $seed."
  pw-build train ../config/symh.yaml \
  -s $seed \
  --wandb \
  -t frankle-prune
done
