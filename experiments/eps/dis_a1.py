import os
import wandb
import matplotlib.pyplot as plt
import logging

from pairwise_network import env, wandb_api
from eps_utils import epsv4_cmap


# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------

env.set_log_level()

def filter_runs(runs, filter):
    filtered = runs.copy()
    for key, value in filter.items():
        filtered = wandb_api.filter_runs_by_cfg(filtered, key, value)
    return filtered


tags = ['eps', 'prelim']
version = 'epsv4'
filter = {
    'dataset': version,
    'prune_iterations_max': 0,
    'hidden': [10, 10],
    'weight_decay': 0,
}
title_prefix = '$a_1$'
cmap = epsv4_cmap
drop_tags = ['r1*', 'bt*', 'v*', 't*']

file_prefix = 'a1'
partition = 'eval'
result_dir = env.results_dir
# ext = '.png'
ext = '.pdf'

save_fig = True
show_fig = True

# -----------------------------------------------------------------------------
# Runs
# -----------------------------------------------------------------------------
runs = wandb_api.get_runs_filtered_by_tag(tags)
logging.info('Filtering runs by config')

print(f'Number of runs: {len(runs)}')

# -----------------------------------------------------------------------------
# Baseline (Duplicated Init)
# -----------------------------------------------------------------------------

filter['init_scheme'] = 'identical_subnets'
filter['lr'] = 0.001
filtered_runs = filter_runs(runs, filter)
print(f'Number of runs: {len(filtered_runs)}')

fig, ax = plt.subplots(1, figsize=(3.2, 2.5))
# fig, ax = plt.subplots(1, figsize=(2, 2.5))
# ax.set_title(title_prefix + '\nDuplicate Init')
ax.set_title(title_prefix)
wandb_api.sort_runs_by_config_key(filtered_runs, 'seed', reverse=False)
wandb_api.plot_runs_sn_act_dist(filtered_runs, partition=partition,
    rename_subnets=True, cmap=cmap, ax=ax, legend='right',
    add_xtick_metrics=False, use_symh_tags=True, drop_tags=drop_tags)

ax.set_xlabel('Seed')
fig.tight_layout()

file_path = os.path.join(result_dir, file_prefix + ext)
if save_fig:
    fig.savefig(file_path, facecolor='white')
if show_fig:
    fig.show()

# -----------------------------------------------------------------------------
# Random Init
# -----------------------------------------------------------------------------
# filter['init_scheme'] = 'random'
# filter['lr'] = 0.001
# filtered_runs = filter_runs(runs, filter)
# print(f'Number of runs: {len(filtered_runs)}')
#
# # fig, ax = plt.subplots(1, figsize=(2.8, 2.5))
# # fig, ax = plt.subplots(1, figsize=(2.3, 2.5))
# fig, ax = plt.subplots(1, figsize=(3.2, 2.5))
# # fig, ax = plt.subplots(1, figsize=(4, 2.5))
# ax.set_title(title_prefix + '\nRandom Init')
# wandb_api.sort_runs_by_config_key(filtered_runs, 'seed', reverse=False)
# wandb_api.plot_runs_sn_act_dist(filtered_runs, partition=partition,
#     rename_subnets=True, cmap=cmap, ax=ax, legend='right',
#     add_xtick_metrics=False, use_symh_tags=True, drop_tags=drop_tags)
#
# ax.set_xlabel('Seed')
# fig.tight_layout()
#
# file_path = os.path.join(result_dir, file_prefix + '-random' + ext)
# if save_fig:
#     fig.savefig(file_path, facecolor='white')
# if show_fig:
#     fig.show()

# -----------------------------------------------------------------------------
# Prune
# -----------------------------------------------------------------------------

# tags = ['eps', 'prune-no-des']
# filter = {
#     'dataset':version,
#     'hidden': [10,10],
#     'weight_decay': 0,
#     'lr': 0.001,
#     'prune_amount': 0.1,
#     'init_scheme': 'identical_subnets'
# }
# runs = wandb_api.get_runs_filtered_by_tag(tags)
# filtered_runs = filter_runs(runs, filter)
# print(f'Number of runs: {len(filtered_runs)}')
#
# # fig, ax = plt.subplots(1, figsize=(5, 6))
# fig, ax = plt.subplots(1, figsize=(3.5, 2.5))
# ax.set_title(title_prefix + ' (Pruned)')
# wandb_api.sort_runs_by_config_key(filtered_runs, 'seed', reverse=False)
# wandb_api.plot_runs_sn_act_dist(filtered_runs, partition=partition,
#     rename_subnets=True, cmap=cmap, ax=ax, legend='right',
#     add_xtick_metrics=False, use_symh_tags=True, drop_tags=drop_tags)
#
# file_path = os.path.join(result_dir, file_prefix + '-prune' + ext)
# fig.savefig(file_path, facecolor='white')
