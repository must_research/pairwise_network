import os
import pandas as pd
import wandb
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import logging
from matplotlib import dates as mdates
from sklearn import preprocessing

from pairwise_network import env, wandb_api, dataset
from symh_dataset.env import storm_patrick


run_path = 'jpbeukes27/pairwise/9tjrr7xe'

run = wandb_api.run_path_to_run(run_path)
cfg = wandb_api.run_to_cfg(run)

model, _, _ = wandb_api.run_to_model(run)
model.eval()

(df_train, df_valid, df_eval) = dataset.read_df_datasets(cfg['dataset'], env.data_dir)
x_scaler, y_scaler = dataset.df_to_scaler(df_train, cfg['inputs'], cfg['outputs'])
df_valid = df_valid.dropna()

(train_set, valid_set, eval_set), _ = dataset.create_dataset_from_cfg(cfg)

x = valid_set.data
x = x.to(cfg['device'])
y_pred = model(x).cpu().detach().numpy().squeeze()
y_pred = y_scaler.inverse_transform(y_pred)

# y_true = y_scaler.transform(df_valid['e*'].values.reshape(-1, 1)).squeeze()
y_true = df_valid['e*'].values
result_df = pd.DataFrame({'true': y_true, 'pred': y_pred}, index=df_valid.index)

storm_df = result_df.loc['2015-03-17': '2015-03-18']
fig, ax = plt.subplots(figsize=(8, 2))

ax.plot(storm_df['true'])
    # marker='.',
    # markersize=2,
    # markerfacecolor=(0, 0, 0, 1),
    # color=(148/255, 194/255, 183/255, 1),
    # color=(148/255, 194/255, 183/255, 0.3),
# )
ax.plot(storm_df['pred'])
    # '.',
    # markersize=5,
    # markerfacecolor=(0, 0, 0, 1),
    # color=(244/255, 186/255, 108/255, 1))
ax.ticklabel_format(axis='y', style='sci', scilimits=(2, 4))
# storm_df.plot(marker='.', markersize=5, ax=ax, markerfacecolor=(r, g, ,0.5))
# storm_df['e*'].plot(style='.', markersize=1, ax=ax)
# # storm_df.plot(style='.', markersize=1, subplots=True)

locator = mdates.AutoDateLocator(minticks=2, maxticks=3)
formatter = mdates.ConciseDateFormatter(locator)
ax.xaxis.set_major_locator(locator)
ax.xaxis.set_major_formatter(formatter)

ax.legend(['Observed', 'Predicted'])
ax.set_ylabel('$\epsilon^*$', rotation=0)
ax.get_yaxis().set_label_coords(-0.08, 0.5)
fig.tight_layout()

file_path = os.path.join(env.results_dir, 'epsilon-predict.pdf')
fig.savefig(file_path, facecolor='white')
# fig.show()
