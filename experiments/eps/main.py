import numpy as np
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
from sklearn import preprocessing
import torch
import sympy

from pairwise_network.analyse.activation import calc_plane_act
from pairwise_network.art.art_core import calc_target_plane
from pairwise_network import wandb_api, dataset, env

env.set_log_level()

run = wandb_api.run_id_to_run('f4b0pue2')

cfg = wandb_api.run_to_cfg(run)
(train_df, _, _) = dataset.read_df_datasets(cfg['dataset'], env.data_dir)
(trainset, _, _) = dataset.create_dataset_from_cfg(cfg)
input_tags = cfg['inputs']
x_scaler, y_scaler = dataset.df_to_scaler(train_df, input_tags, cfg['outputs'])
model, _, _ = wandb_api.run_to_model(run)

n_samples = 1500
x = trainset.data[:n_samples]
y_pred = model(x).detach().cpu().numpy()
y_true = trainset.targets[:n_samples].detach().cpu().numpy()

idx = train_df[:n_samples].index
fig, axs = plt.subplots(6, 1, sharex='col', figsize=(10, 10))
ax1 = axs[0]
ax1.plot(idx, train_df[cfg['outputs']].values[:n_samples])
ax1.plot(idx, y_scaler.inverse_transform(y_pred))
ax1.legend(['Target', 'Model'])
fig.suptitle(r'$\epsilon^* = V_{sw} B_T^2 \sin^4(\frac{\theta}{2})$', fontsize=14)


(train_df, _, _) = dataset.read_df_datasets('epsv3', env.data_dir)

# latex_tags = ['v', r'B_T', r'\theta', 'r \sim \mathcal{N}(0, 1)']
latex_tags = ['v', 'B_T', 'B_Y', 'B_Z', 'r \sim \mathcal{N}(0, 1)']
input_tags = ['v', 'bt', 'by', 'bz', 'r1']
for i in range(len(input_tags)):
    ax = axs[i + 1]
    values = train_df[input_tags[i]].values[:n_samples]
    ax.plot(idx, values, '.', markersize=1)
    ax.set_title(f'${latex_tags[i]}$',fontsize=14)

locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
formatter = mdates.ConciseDateFormatter(locator)
ax1.xaxis.set_major_locator(locator)
ax1.xaxis.set_major_formatter(formatter)

fig.subplots_adjust(hspace=0)

fig.show()

# act_dist = wandb_api.run_to_sn_act_dist(run)