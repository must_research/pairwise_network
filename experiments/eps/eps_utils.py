import numpy as np

from matplotlib.colors import ListedColormap

epsv4_cmap = ListedColormap([
    'tab:blue',
    np.divide((113, 207, 150), 255),
    'tab:orange',
    'tab:purple',
    np.divide((248, 144, 147), 255),
    np.divide((242, 33, 39), 255),
    np.divide((182, 222, 239), 255),
    'tab:gray',
    (30 / 255, 180 / 255, 220 / 255),
    np.divide((179, 0, 8), 255),
])

epsv5_cmap = ListedColormap([
    'tab:blue',                         # v, bt
    np.divide((113, 207, 150), 255),    # v, by
    'tab:purple',                       # v, bz
    'tab:orange',                       # v, r
    np.divide((182, 222, 239), 255),    # bt, by
    np.divide((100, 137, 163), 255),    # bt, bz
    np.divide((248, 144, 147), 255),    # bt, r
    np.divide((30, 180, 220), 255),     # by, bz
    'tab:red',                          # by, r
    np.divide((255, 118, 163), 255),     # bz, r
    np.divide((211, 235, 105), 255),    # v
    np.divide((62, 55, 94), 255),       # bt
    np.divide((216, 203, 225), 255),    # by
    np.divide((216, 243, 197), 255),    # bz
    'tab:brown',                        # r
])

