import os

import matplotlib

# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import logging

from pairwise_network import wandb_api, env, labels

from eps_utils import epsv4_cmap, epsv5_cmap

env.set_log_level()

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------
env.set_log_level()

d = 'epsv4'
# d = 'epsv5'
datasets = ['epsv4', 'epsv5']
save_map = dict(zip(datasets, ('a1', 'a2')))
figsize_map = dict(zip(datasets, ((3.5, 2.5),(4.5, 2.5))))
titles = dict(zip(datasets, ('$a_1$', '$a_2$')))
ncol_map = dict(zip(datasets, (1, 2)))
drop_tags_dict = dict(zip(datasets, (
    # None,
    # ('r1*',),
    ('r1*', 'bt*', 'v*'),
    # None,
    ('r1*', 'bz*', 'bt*')
)))
cmap_dict = dict(zip(datasets, (epsv4_cmap, epsv5_cmap)))
partition = 'eval'

# tags = ['prune-des', 'optimised']
tags = ['prune-no-des', 'optimised']

filter = {
    'dataset': d,
    # 'prune_constant_epochs': True
    'prune_amount': 0.1
}

result_dir = env.results_dir

save_plt = False
# save_plt = True

# -----------------------------------------------------------------------------
# Runs
# -----------------------------------------------------------------------------
runs = wandb_api.get_runs_filtered_by_tag(tags)
filtered = wandb_api.filter_runs(runs, filter)
logging.info(f'Number of runs: {len(runs)}')

# -----------------------------------------------------------------------------
# Plot
# -----------------------------------------------------------------------------
# figsize = (4, 6)
# figsize = (2, 2.5)
# figsize = (2.5, 6)
fig, ax = plt.subplots(1, figsize=figsize_map[d])

add_metrics = False

wandb_api.sort_runs_by_config_key(filtered, 'seed', reverse=False)
if len(filtered) == 0:
    logging.warning(f'No runs found for {d}')
else:
    legend = 'right'
    # legend = 'none'

    wandb_api.plot_runs_sn_act_dist(filtered, partition=partition,
        rename_subnets=True, cmap=cmap_dict[d], ax=ax, legend=legend,
        add_xtick_metrics=add_metrics, metric_format='%.2f',
        use_symh_tags=True,
        drop_tags=drop_tags_dict[d],
        ncols=ncol_map[d])
    ax.set_title(titles[d])
    ax.set_xlabel('Seed')
fig.tight_layout()
# -----------------------------------------------------------------------------
# Save and Show
# -----------------------------------------------------------------------------
if save_plt:
    filename = f'prune-{save_map[d]}.pdf'
    fig.savefig(os.path.join(env.results_dir, filename))
else:
    fig.show()
