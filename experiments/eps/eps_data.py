import os
import pandas as pd
import numpy as np
import matplotlib

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import logging
from matplotlib import dates as mdates

from pairwise_network import dataset, env
from symh_dataset.env import storm_patrick, phases
from symh_dataset.analyse.storms import plot_symh_storm
import symh_dataset
from symh_dataset.labels import list_to_latex

data_dir = env.data_dir
(_, df_eps4, _) = dataset.read_df_datasets('epsv4', data_dir)
(_, df_eps5, _) = dataset.read_df_datasets('epsv5', data_dir)
df_symhv3 = symh_dataset.create_df_entire_dataset('symhv3', data_dir,
    drop_missing=False, standardize=False, include_phase=True, encode_phase=False)

t0 = storm_patrick[0]
# t1 = storm_patrick[1]
t1 = '2015-03-18'
n_samples = 6000
alpha = 1
left_label_xcord = -0.15
right_label_xcord = 1.15
markersize = 1

df_eps4 = df_eps4.loc[t0: t1].iloc[:n_samples]
df_eps5 = df_eps5.loc[t0: t1].iloc[:n_samples]
symh = df_symhv3.loc[t0: t1].symh
phs = df_symhv3.loc[t0: t1].phs

df = pd.DataFrame({
    'v': df_eps4['v'].values,
    'bt': df_eps4['bt'].values,
    't': df_eps4['t'].values,
    'r1': df_eps4['r1'].values,
    'by': df_eps5['by'].values,
    'bz': df_eps5['bz'].values,
    'e*': df_eps4['e*'].values}, index=df_eps4.index)
latex_tags = list_to_latex(df.columns.to_list(), add_units=True)
latex_dict = dict(zip(df.columns, latex_tags))

fig, axs = plt.subplots(5, 1, sharex='all', figsize=(8, 6), gridspec_kw={
    'height_ratios': [0.3, 0.2, 0.2, 0.15, 0.15]})

# Epsilon
ax = axs[0]
ax.plot(df['e*'], '.', color='black', markersize=markersize, alpha=alpha)
ax.set_ylabel('$\epsilon^*$', color='black', rotation=0, ha='center',
    va='center')
ax.get_yaxis().set_label_coords(left_label_xcord, 0.5)
ax.ticklabel_format(axis='y', style='sci', scilimits=(2, 4))
ax.set_ylim(ymax=1.8e5)

# Symh
ax_symh = ax.twinx()
ax_symh.set_ylim((-250, 150))
# plot_symh_storm(symh.values, symh.index, ax_symh, True, add_th=False,
#     color=('b', 'r', 'orange'), leg_loc='center right')
ax_symh.plot(symh, '.', color='r', markersize=markersize, alpha=alpha)
ax_symh.set_ylabel('SYM-H\n[nT]', color='r', rotation=0, ha='center', va='center')
ax_symh.tick_params(axis='y', labelcolor='r')
ax_symh.get_yaxis().set_label_coords(right_label_xcord, 0.5)

# Vsw
ax = axs[1]
ax.plot(df['v'], '.', color='black', markersize=markersize, alpha=alpha)
ax.set_ylabel(latex_dict['v'], color='black', rotation=0, ha='center',
    va='center')
ax.get_yaxis().set_label_coords(left_label_xcord, 0.5)

# Bt
ax = ax.twinx()
ax.plot(df['bt'], '.', color='r', markersize=markersize, alpha=alpha)
ax.set_ylabel(latex_dict['bt'], color='r', rotation=0, ha='center', va='center')
ax.tick_params(axis='y', labelcolor='r')
ax.get_yaxis().set_label_coords(right_label_xcord, 0.5)

# By
ax = axs[2]
ax.plot(df['by'], '.', color='black', markersize=markersize, alpha=alpha)
ax.set_ylabel(latex_dict['by'], color='black', rotation=0, ha='center',
    va='center')
ax.get_yaxis().set_label_coords(left_label_xcord, 0.5)

# Bz
ax = ax.twinx()
ax.plot(df['bz'], '.', color='r', markersize=markersize, alpha=alpha)
ax.set_ylabel(latex_dict['bz'], color='r', rotation=0, ha='center', va='center')
ax.tick_params(axis='y', labelcolor='r')
ax.get_yaxis().set_label_coords(right_label_xcord, 0.5)

# theta
ax = axs[3]
ax.plot(df['t'] * 180 / np.pi, '.', color='black', markersize=markersize,
    alpha=alpha)
ax.set_ylabel(latex_dict['t'] + '[deg]', color='black', rotation=0, ha='center',
    va='center')
ax.get_yaxis().set_label_coords(left_label_xcord, 0.5)

# r
ax = axs[4]
ax.plot(df['r1'], '.', color='black', markersize=markersize, alpha=alpha)
ax.set_ylabel('$r$', color='black', rotation=0, ha='center',
    va='center')
ax.get_yaxis().set_label_coords(left_label_xcord, 0.5)

# Dates
locator = mdates.AutoDateLocator(minticks=2, maxticks=3)
formatter = mdates.ConciseDateFormatter(locator)
axs[-1].xaxis.set_major_locator(locator)
axs[-1].xaxis.set_major_formatter(formatter)

# Phases
storm_end_idx = pd.Index([phs.index[-1], ])
phs_start = pd.Series(phs != phs.shift())  # mark start of a new phase
phs_start_idx = phs_start[phs_start == 1].index

switch_idx = phs_start_idx.append(storm_end_idx)
caption_idx = switch_idx[:-1] + (switch_idx[1:] - switch_idx[:-1]) / 2

for i, tag in enumerate(phases):
    ax_symh.text(caption_idx[i], 100, tag.capitalize(),
        horizontalalignment='center')

# Phase vertical lines
for ax in axs:
    for idx in phs_start_idx[1:]:
        ax.axvline(x=idx, linestyle='dashed', alpha=0.5)
    # ax.axvline(x=storm_end_idx, linestyle='dashed', alpha=0.5)

# Show and save
fig.tight_layout()
file_path = os.path.join(env.results_dir, 'epsilon-data.pdf')
fig.savefig(file_path, facecolor='white')
