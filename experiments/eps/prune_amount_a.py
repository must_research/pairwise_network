import os

import matplotlib

# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import logging

from pairwise_network import wandb_api, env, labels

from eps_utils import epsv4_cmap, epsv5_cmap

env.set_log_level()

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------
env.set_log_level()

prune_amounts = (0.1, 0.15, 0.2, 0.25, 0.3)
columns = ['prune-des', 'prune-no-des']
datasets = ['epsv4', 'epsv5']
# d = 'epsv4'
d = 'epsv5'
ds_map = dict(zip(datasets, ('a_1', 'a_2')))
titles = dict(zip(columns, (f'${ds_map[d]}$\nFine-tune to\ncompletion',
                            f'${ds_map[d]}$\nFine-tune for\n100 epochs')))
drop_tags_dict = dict(zip(datasets, (
    None,
    # ('r1*',),
    # ('r1*', 'bt*', 'v*', 't*'),
    None,
    # ('r1*', 'bz*', 'bt*')
)))
cmap_dict = dict(zip(datasets, (epsv4_cmap, epsv5_cmap)))
partition = 'eval'

# tags = ['prune-des', 'optimised']
# tags = ['prune-no-des', 'optimised']

# filter = {
#     'dataset': 'epsv4',
#     'prune_constant_epochs': True
# }

result_dir = env.results_dir

# save_plt = False
save_plt = True

# -----------------------------------------------------------------------------
# Runs
# -----------------------------------------------------------------------------
# runs = wandb_api.filter_runs(runs, filter)
# logging.info(f'Number of runs: {len(runs)}')

# -----------------------------------------------------------------------------
# Plot
# -----------------------------------------------------------------------------
figsize = (4, 6)
# figsize = (2, 6)
# figsize = (2.5, 6)
fig, axs = plt.subplots(len(prune_amounts), len(columns), figsize=figsize,
    # gridspec_kw={'wspace': 0.1,
    #              'hspace': 0.5,
    #              'width_ratios': [0.55, 0.45, ],
    # }
)
# axs = axs.flatten()
for c, t in enumerate(columns):
    tags = [t, 'optimised']
    runs = wandb_api.get_runs_filtered_by_tag(tags)
    for k, p in enumerate(prune_amounts):
        ax = axs[k, c]
        add_metrics = False
        if c == 0:
            ax.set_ylabel(f'$p={int(p * 100)}$', rotation=0, ha='center',
                va='center')
            ax.get_yaxis().set_label_coords(-0.15, 0.5)
            # add_metrics = True

        filtered = wandb_api.filter_runs_by_cfg(runs, 'prune_amount', p)
        filtered = wandb_api.filter_runs_by_cfg(filtered, 'dataset', d)
        wandb_api.sort_runs_by_config_key(filtered, 'seed', reverse=False)
        if len(filtered) == 0:
            logging.warning(f'No runs found for {d}, {p}')
        else:
            legend = 'bottom'
            # legend = 'none'
            # if c == len(columns) - 1 and k == len(prune_amounts) - 1:
            #     legend = 'bottom'

            wandb_api.plot_runs_sn_act_dist(filtered, partition=partition,
                rename_subnets=True, cmap=cmap_dict[d], ax=ax, legend=legend,
                add_xtick_metrics=add_metrics, metric_format='%.2f',
                use_symh_tags=True,
                # drop_tags=None,
                drop_tags=drop_tags_dict[d],
                ncols=4)
            if k == 0:
                ax.set_title(titles[t])
            if k != len(prune_amounts) - 1:
                ax.set_xticks([])
fig.tight_layout()
# -----------------------------------------------------------------------------
# Save and Show
# -----------------------------------------------------------------------------
if save_plt:
    filename = 'legend.pdf'
    # filename = 'prune-p-des-a1.pdf'
    # filename = 'prune-p-a2.pdf'
    fig.savefig(os.path.join(env.results_dir, filename))
else:
    fig.show()
