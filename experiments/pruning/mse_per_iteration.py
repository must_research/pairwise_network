import logging
import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, StrMethodFormatter

from pairwise_network import env, wandb_api

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------
env.set_log_level()

tags = ['prune', ]
# tags = ['prune-no-des', 'eps']

filter = {
    # 'dataset': 'epsv4',
    # 'dataset': 'epsv5',
    'dataset': 'symhv3',
}
seeds = None
figsize = (4, 2)
if filter['dataset'] == 'epsv4':
    file_prefix = 'a1'
    title_prefix = '$a_1$'
    filter['lr'] = 0.001
    tick_multiple = 2
elif filter['dataset'] == 'epsv5':
    file_prefix = 'a2'
    title_prefix = '$a_2$'
    filter['lr'] = 0.001
    tick_multiple = 3
else:
    file_prefix = 'symh'
    title_prefix = 'SYM-H'
    filter['lr'] = 0.00005
    filter['phase'] = 'All'
    filter['timeshift'] = [270, ]
    tick_multiple = 3
    seeds = [1, 2, 3]
    figsize = (6, 5)

# tags = ['prune-no-des', 'snd', 'art']
#
# filter = {
#     'dataset': 'art-snd',
#     # 'art_function': 'x1+x2',
#     'art_function': 'x1*x2',
# }
# if filter['art_function'] == 'x1+x2':
#     file_prefix = 'y1'
#     title_prefix = '$y_1$'
#     filter['lr'] = 0.1
# else:
#     file_prefix = 'y2'
#     title_prefix = '$y_2$'
#     filter['lr'] = 0.01

result_dir = env.results_dir

# ext = '.png'
ext = '.pdf'

save_plt = False
# save_plt = True

# -----------------------------------------------------------------------------
# Runs
# -----------------------------------------------------------------------------

runs = wandb_api.get_runs_filtered_by_tag(tags)
logging.info('Filtering runs by config')
runs = wandb_api.filter_runs(runs, filter)
logging.info(f'Number of runs: {len(runs)}')

# -----------------------------------------------------------------------------
# Data
# -----------------------------------------------------------------------------
mse_dict = {}
for run in runs:
    df = wandb_api.run_to_prune_df(run)
    mse_dict[run.config['seed']] = df.loc[:, 'valid_loss'].values
df_mse = pd.DataFrame.from_dict(mse_dict, orient='index')
df_mse = df_mse.transpose()

# -----------------------------------------------------------------------------
# Plot
# -----------------------------------------------------------------------------
fig, ax = plt.subplots(figsize=figsize)
x = list(range(len(df_mse)))
if seeds is not None:
    df_mse = df_mse[seeds]

df_mse.plot(marker='.', ax=ax)

for i in range(df_mse.shape[1]):
    y = df_mse.iloc[:, i]
    thres = y.min() + 0.02
    # ax.fill_between(x, y, thres, alpha=0.2)
    ax.axhline(thres, alpha=0.2)

plt.ylabel('Validation MSE')
plt.xlabel('Prune Iteration')

locator = MultipleLocator(tick_multiple)
plt.gca().xaxis.set_major_locator(locator)
formatter = StrMethodFormatter("{x:.0f}")
plt.gca().xaxis.set_major_formatter(formatter)

# plt.ylim((0, 0.7))

plt.title(title_prefix)
plt.legend(title='Seed', ncol=2, loc='upper left')
plt.tight_layout()

# -----------------------------------------------------------------------------
# Save and Show
# -----------------------------------------------------------------------------
if save_plt:
    file_path = os.path.join(result_dir, f'mse_iter-{file_prefix}{ext}')
    plt.savefig(file_path)
else:
    plt.show()
