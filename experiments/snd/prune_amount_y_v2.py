import os

import matplotlib

# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import logging

from pairwise_network import wandb_api, env, labels

from exp_utils import snd_cmap

env.set_log_level()

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------
env.set_log_level()

prune_amounts = (0.1, 0.15, 0.2, 0.25, 0.3)
columns = ['prune-des', 'prune-no-des']
functions = ('x1+x2', 'x1*x2')
# f = 'x1+x2'
f = 'x1*x2'
f_map = dict(zip(functions, ('y_1', 'y_2')))
titles = dict(zip(columns, (f'${f_map[f]}$\nFine-tune to\ncompletion',
                            f'${f_map[f]}$\nFine-tune for\n100 epochs')))
partition = 'eval'
drop_tags = ['x3*', 'x4*']

result_dir = env.results_dir

# save_plt = False
save_plt = True


# -----------------------------------------------------------------------------
# Plot
# -----------------------------------------------------------------------------
fig, axs = plt.subplots(len(prune_amounts), len(columns), figsize=(4, 6),
    # gridspec_kw={'wspace': 0.1,
    #              'hspace': 0.5,
    #              'width_ratios': [0.55, 0.45, ],
    # }
)
for c, t in enumerate(columns):
    tags = [t, 'optimised']
    runs = wandb_api.get_runs_filtered_by_tag(tags)
    # logging.info('Filtering runs by config')
    # runs = wandb_api.filter_runs(runs, filter)
    # logging.info(f'Number of runs: {len(runs)}')
    for k, p in enumerate(prune_amounts):
            ax = axs[k, c]
            add_metrics = False
            if c == 0:
                ax.set_ylabel(f'$p={int(p * 100)}$', rotation=0, ha='center',
                    va='center')
                ax.get_yaxis().set_label_coords(-0.15, 0.5)
                # add_metrics = True

            filtered = wandb_api.filter_runs_by_cfg(runs, 'prune_amount', p)
            filtered = wandb_api.filter_runs_by_cfg(filtered, 'art_function', f)
            wandb_api.sort_runs_by_config_key(filtered, 'seed', reverse=False)
            if len(filtered) == 0:
                logging.warning(f'No runs found for {f}, {p}')
            else:
                legend = 'none'
                # if i == len(axs) - 1:
                #     legend = 'bottom'

                wandb_api.plot_runs_sn_act_dist(filtered, partition=partition,
                    rename_subnets=True, cmap=snd_cmap, ax=ax, legend=legend,
                    add_xtick_metrics=add_metrics, metric_format='%.2f',
                    drop_tags=drop_tags, ncols=4)
                if k == 0:
                    ax.set_title(titles[t])
                if k != len(prune_amounts) - 1:
                    ax.set_xticks([])

# -----------------------------------------------------------------------------
# Save and Show
# -----------------------------------------------------------------------------
if save_plt:
    # filename = 'legend.pdf'
    filename = f'prune-p-y2.pdf'
    fig.savefig(os.path.join(env.results_dir, filename))
else:
    fig.show()
