import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os
import sympy

from pairwise_network import wandb_api, env, net, labels
from pairwise_network.art.art_core import calc_target_plane
from pairwise_network.analyse.activation import calc_plane_act


def run_data_plane(run):
    # TODO: rethink the purpose of this function.
    # plane act
    cfg = wandb_api.run_to_cfg(run)

    eq = sympy.sympify(cfg['art_function'])

    x1 = np.linspace(0, 1, 10)
    x2 = np.linspace(0, 1, 10)

    fig, axs = plt.subplots(1, 2, figsize=(15, 10), gridspec_kw={'wspace': 0.4,
        'hspace': 0.3})
    axs = axs.flatten()
    for i in range(len(eq.args)):
        f = eq.args[i]
        symbols = list(f.atoms(sympy.Symbol))
        if len(symbols) == 2:
            y = calc_target_plane(str(f), x1, x2)
            ax = axs[i]
            sns.heatmap(y, ax=ax)
            ax.set_title(f'${sympy.latex(f)}$')
            ax.set_ylabel(symbols[0])
            ax.set_xlabel(symbols[1])
    fig.suptitle(f'Ground truth.')
    file_name = os.path.join(env.results_dir, 'data-plane.png')
    fig.savefig(file_name)
    fig.show()


def run_model_plane_act(run):
    cfg = wandb_api.run_to_cfg(run)
    model, pairwise_structure, _ = wandb_api.run_to_model(run, reload=False)

    act_dist = wandb_api.run_to_sn_act_dist(run, rename_subnets=False)
    input_tags = cfg['inputs']
    ren_tags = net.rename_subnets_based_on_lin_2(model, pairwise_structure,
        input_tags)

    x = np.linspace(0, 1, 10)
    subnet_tags = list(pairwise_structure.keys())
    n_subnets = len(subnet_tags)
    # fig, axs = plt.subplots(2, 3, figsize=(10, 6), gridspec_kw={'wspace': 0.4,
    #     'hspace': 0.4})
    fig, axs = plt.subplots(3, 5, figsize=(20, 12),gridspec_kw={'wspace': 0.4,
        'hspace': 0.3})
    axs = axs.flatten()
    for i in range(n_subnets):
        subnet_tag = subnet_tags[i]
        tags = subnet_tag.split('-')
        y = calc_plane_act(model, x1=x, x2=x, x1_tag=tags[0], x2_tag=tags[1],
            input_tags=input_tags, subnet_idx=i)
        ax = axs[i]
        sns.heatmap(y, ax=ax)
        ax.set_ylabel(tags[0])
        ax.set_xlabel(tags[1])
        ax.set_title(f'{ren_tags[i]} ({act_dist[subnet_tag] * 100:.2f}%)')

    f = labels.sympy_func_to_latex(cfg['art_function'])
    fig.suptitle(f'Weighted activation of each subnet for a grid of inputs,\n'
                 f'y=${f}$, seed={cfg["seed"]}')
    file_name = os.path.join(env.results_dir,
        f'act-plane-seed{cfg["seed"]}-{run.id}.png')
    fig.savefig(file_name)
