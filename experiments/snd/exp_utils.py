
from matplotlib.colors import ListedColormap

snd_cmap = ListedColormap([
    'tab:blue',
    'tab:orange',
    'tab:green',
    'tab:red',
    'tab:purple',
    'tab:pink',
    (30 / 255, 180 / 255, 220 / 255),
    (130 / 255, 228 / 255, 255 / 255),
    'tab:gray',
    'tab:brown',
])