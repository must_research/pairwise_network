import os
import matplotlib
# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import logging

from pairwise_network import env, wandb_api
from exp_utils import snd_cmap

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------

env.set_log_level()

func = 'x1+x2'
# func = 'x1*x2'

tags = ['prune-no-des', 'optimised']

filter = {
    'dataset': 'art-snd',
    'art_function': func,
    # 'hidden': [10, 10],
    # 'weight_decay': 0,
    # 'batch_size': 64,
    # 'prune_tolerance': 0.05,
    'prune_amount': 0.1,
    # 'lr': 0.01
}
if func == 'x1+x2':
    file_prefix = 'y1'
    title_prefix = '$y_1$'
    legend = 'none'
    figsize = (2.2, 2.5)
else:
    file_prefix = 'y2'
    title_prefix = '$y_2$'
    legend = 'right'
    figsize = (3.2, 2.5)
partition = 'eval'
drop_tags = ['x3*', 'x4*']
result_dir = env.results_dir

# ext = '.png'
ext = '.pdf'

save_fig = True
show_fig = True

# -----------------------------------------------------------------------------
# Prune
# -----------------------------------------------------------------------------


runs = wandb_api.get_runs_filtered_by_tag(tags)
filtered_runs = wandb_api.filter_runs(runs, filter)
print(f'Number of runs: {len(filtered_runs)}')

# fig, ax = plt.subplots(1, figsize=(5, 4))
# fig, ax = plt.subplots(1, figsize=(2.2, 2.5))
fig, ax = plt.subplots(1, figsize=figsize)
ax.set_title(title_prefix)
wandb_api.sort_runs_by_config_key(filtered_runs, 'seed', reverse=False)
wandb_api.plot_runs_sn_act_dist(filtered_runs, partition=partition,
    rename_subnets=True, cmap=snd_cmap, ax=ax, legend=legend,
    add_xtick_metrics=False, metric_format='%.2f', drop_tags=drop_tags)

ax.set_xlabel('Seed')
fig.tight_layout()

file_path = os.path.join(result_dir, 'prune-' + file_prefix + ext)
if save_fig:
    fig.savefig(file_path, facecolor='white')
if show_fig:
    fig.show()
