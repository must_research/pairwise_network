# %%

import os
import matplotlib
# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import logging

from pairwise_network import env, wandb_api
from exp_utils import snd_cmap

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------

# env.set_log_level()

# func = 'x1+x2'
func = 'x1*x2'

tags = ['snd', 'art', 'prelim']

filter = {
    'dataset': 'art-snd',
    'art_function': func,
    'prune_iterations_max': 0,
    'hidden': [10, 10],
    'weight_decay': 0,
    'batch_size': 64,
}
if func == 'x1+x2':
    file_prefix = f'y1'
    title_prefix = '$y_1$'
else:
    file_prefix = f'y2'
    title_prefix = '$y_2$'
partition = 'eval'
# drop_tags = ['x3*', 'x4*']
drop_tags = ['x1*', 'x2*', 'x3*', 'x4*']
result_dir = env.results_dir

# ext = '.png'
ext = '.pdf'

# -----------------------------------------------------------------------------
# Runs
# -----------------------------------------------------------------------------

runs = wandb_api.get_runs_filtered_by_tag(tags)
logging.info('Filtering runs by config')

print(f'Number of runs: {len(runs)}')

# -----------------------------------------------------------------------------
# Baseline (Duplicated Init)
# -----------------------------------------------------------------------------

filter['init_scheme'] = 'identical_subnets'
filter['lr'] = 0.1 if func == 'x1+x2' else 0.01
filtered_runs = wandb_api.filter_runs(runs, filter)
print(f'Number of runs: {len(filtered_runs)}')

fig, ax = plt.subplots(1, figsize=(3.2, 2.5))
# fig, ax = plt.subplots(1, figsize=(2.2, 2.5))
ax.set_title(title_prefix, pad=5)
# ax.set_title(title_prefix + '\nDuplicated Init', pad=5)
wandb_api.sort_runs_by_config_key(filtered_runs, 'seed', reverse=False)
wandb_api.plot_runs_sn_act_dist(filtered_runs, partition=partition,
    rename_subnets=True, cmap=snd_cmap, ax=ax, legend='right',
    add_xtick_metrics=False, metric_format='%.2f', drop_tags=drop_tags)
ax.set_xlabel('Seed')
fig.tight_layout()

file_path = os.path.join(result_dir, file_prefix + ext)
fig.savefig(file_path, facecolor='white')
fig.show()

# -----------------------------------------------------------------------------
# Random Init
# -----------------------------------------------------------------------------

# filter['init_scheme'] = 'random'
# filter['lr'] = 0.1 if func == 'x1+x2' else 0.05
# filtered_runs = wandb_api.filter_runs(runs, filter)
# print(f'Number of runs: {len(filtered_runs)}')
#
# # fig, ax = plt.subplots(1, figsize=(2.8, 2.5))
# # fig, ax = plt.subplots(1, figsize=(2.2, 2.5))
# fig, ax = plt.subplots(1, figsize=(3, 2.5))
# ax.set_title(title_prefix + '\nRandom Init', pad=8)
# wandb_api.sort_runs_by_config_key(filtered_runs, 'seed', reverse=False)
# wandb_api.plot_runs_sn_act_dist(filtered_runs, partition=partition,
#     rename_subnets=True, cmap=snd_cmap, ax=ax, legend='right',
#     add_xtick_metrics=False, metric_format='%.2f', drop_tags=drop_tags,
#     bbox_to_anchor=(1, 1))
# ax.set_xlabel('Seed')
# fig.tight_layout()
# file_path = os.path.join(result_dir, file_prefix + '-random' + ext)
# fig.savefig(file_path, facecolor='white')
# fig.show()
