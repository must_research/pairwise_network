import os

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from pairwise_network import wandb_api, dataset, metrics, env, labels


run_id = 'k0jhnbi5'
# run_id = 'ks5lpeqo'
# run_id = 'wgmzjhv9'
# run_id = 'fjtivdq2'
run = wandb_api.run_id_to_run(run_id)
cfg = wandb_api.run_to_cfg(run)

df_act = wandb_api.run_to_sn_act(run, 'eval')

(_, valid_set, eval_set), input_tags = dataset.create_dataset_from_cfg(cfg)
ds_set = eval_set
targets = ds_set.targets.detach().cpu().numpy()
df_data = pd.DataFrame(ds_set.data.detach().cpu().numpy(), columns=input_tags)

subnet_tags = df_act.columns.to_list()
corr_dict = {}
for s in subnet_tags:
    (p1, p2) = labels.split_pairs(s)
    act = df_act[s]
    corr_dict[s] = {
        p1: metrics.correlation(act, df_data[p1]),
        p2: metrics.correlation(act, df_data[p2])
    }

corr_df = pd.DataFrame(corr_dict)
# corr_df = corr_df.fillna(0)

# # print(f'Correlation with {subnet_tag} and bt: {corr1}')
# # print(f'Correlation with {subnet_tag} and t: {corr2}')
# sns.set_palette("viridis")
# ax = sns.heatmap(corr_df.astype(float), annot=True, vmax=1, vmin=-1, cmap="viridis", fmt='.2f')
# figname = f'a1-baseline-s{cfg["seed"]}-{run_id}.png'
# plt.savefig(os.path.join(env.results_dir, 'corr', figname))
# plt.show()
# plt.close()

filename = f'a1-baseline-s{cfg["seed"]}-{run_id}.csv'
corr_df.to_csv(os.path.join(env.results_dir, filename))
