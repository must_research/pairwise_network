import os
import torch
import matplotlib

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
import logging

import symh_dataset
from symh_dataset import env
from symh_dataset.analyse import storms, phase

from pairwise_network import config, wandb_api, env

# storm_start, storm_end = ('2013-05-30', '2013-06-03')
run_id_list = ('10pgr83m',)

env.set_log_level()


def calc_exp_pred(run_id):
    run = wandb_api.run_id_to_run(run_id)
    cfg = wandb_api.run_to_cfg(run)
    model, _, _ = wandb_api.run_to_model(run)

    use_phase = cfg['phase'] == 'All'

    (_, _, df_eval), _, y_scaler = symh_dataset.create_df_partitions(
        dataset_name, env.data_dir, use_phase, timeshift=cfg['timeshift'],
        interpol_limit=10, standardize=True,
        return_scalers=True)
    data = df_eval.drop(columns='symh').values
    idx = df_eval.index
    x = torch.tensor(data).to(cfg['device']).float()

    y_pred = model(x).detach().cpu().numpy()
    y_pred = y_scaler.inverse_transform(y_pred)
    result = pd.Series(index=idx, data=y_pred.squeeze())
    return result


dataset_name = 'symhv3'
(_, _, df_eval) = symh_dataset.create_df_partitions(dataset_name,
    env.data_dir, include_phase=True, encode_phase=False,
    interpol_limit=10, standardize=False)
df_true = df_eval.copy()
storm_list = storms.get_storm_list(df_true)

storm_list = [storm_list[6], storm_list[9], storm_list[11], ]  # 6, 9, 11
fig, axs = plt.subplots(1, 3, figsize=(9, 3.5), sharey='all')
# axs = [axs, ]

# fig, axs = plt.subplots(4, 4, figsize=(25, 20))
# axs = axs.flatten()

for j, df_storm in enumerate(storm_list):
    storm_start, storm_end = df_storm.index[0], df_storm.index[-1]
    symh = df_true.loc[storm_start: storm_end, 'symh']
    phs = df_true.loc[storm_start: storm_end, 'phs']

    date_idx = symh.index
    y_true = symh.values

    prediction_list = list()

    for run_id in run_id_list:
        prediction_list.append(calc_exp_pred(run_id))

    line_colors = ('k', 'r', 'b')
    # fig, ax = plt.subplots(figsize=(7, 4))
    ax = axs[j]
    ax.plot(date_idx, y_true, color=line_colors[0])
    x_min = pd.Timestamp(storm_start)
    x_max = pd.Timestamp(storm_end)
    for i, y_pred in enumerate(prediction_list):
        y = y_pred.loc[storm_start:storm_end]
        x = y.index
        if x[0] > x_min:
            x_min = x[0]
        if x[-1] < x_max:
            x_max = x[-1]
        ax.plot(x, y.values, color=line_colors[i + 1])

    ax.set_xlim(x_min, x_max)
    ax.set_ylim(ymin=-140, ymax=50)

    phs = phs.loc[x_min:x_max]

    phase.plot_phase_tags(ax, phs, 60, False)

    if j == 2:
        ax.legend(['Observed SYM-H', 'Pairwise Network'], loc='upper right')

    locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
    formatter = mdates.ConciseDateFormatter(locator)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    if j == 0:
        ax.set_ylabel('[nT]', rotation=0)
        ax.get_yaxis().set_label_coords(-0.2, 0.715)
    fig.tight_layout()

fig.show()
# filename = f'{storm_start}.png'
# filename = 'storm-pred-2013.png'
filename = f'pw-eval-storms.pdf'

file_path = os.path.join(env.results_dir, filename)

fig.savefig(file_path)

for i, storm in enumerate(storm_list):
    storm.plot(subplots=True, figsize=(8, 15))
    file_path = os.path.join(env.results_dir, f'storm-{i}.png')
    plt.tight_layout()
    plt.savefig(file_path)
    plt.close()