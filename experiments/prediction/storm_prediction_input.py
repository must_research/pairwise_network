import os
import torch
import matplotlib

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
import logging

import symh_dataset
from symh_dataset import env, labels
from symh_dataset.analyse import storms, phase

from pairwise_network import config, wandb_api, env

# storm_start, storm_end = ('2013-05-30', '2013-06-03')
run_id = '10pgr83m'
dataset_name = 'symhv3'

env.set_log_level()


def calc_exp_pred(run_id):
    run = wandb_api.run_id_to_run(run_id)
    cfg = wandb_api.run_to_cfg(run)
    model, _, _ = wandb_api.run_to_model(run)

    use_phase = cfg['phase'] == 'All'

    (_, _, df_eval), _, y_scaler = symh_dataset.create_df_partitions(
        dataset_name, env.data_dir, use_phase, timeshift=cfg['timeshift'],
        interpol_limit=10, standardize=True,
        return_scalers=True)
    data = df_eval.drop(columns='symh').values
    idx = df_eval.index
    x = torch.tensor(data).to(cfg['device']).float()

    y_pred = model(x).detach().cpu().numpy()
    y_pred = y_scaler.inverse_transform(y_pred)
    result = pd.Series(index=idx, data=y_pred.squeeze())
    return result


(_, _, df_eval) = symh_dataset.create_df_partitions('symhv3',
    env.data_dir, include_phase=True, encode_phase=False,
    interpol_limit=10, standardize=False)
df_true = df_eval.copy()
storm_list = storms.get_storm_list(df_true)

storm_list = [storm_list[9], storm_list[11]]
# storm = storm_list[11]
# storm_list = [storm_list[6], storm_list[9], storm_list[11], ]  # 6, 9, 11

pred = calc_exp_pred(run_id)

fig, axs = plt.subplots(9, 2, figsize=(9, 10), sharex='col', sharey='row')

for s, storm in enumerate(storm_list):
    axs_col = axs[:, s]
    storm_start, storm_end = storm.index[0], storm.index[-1]
    phs = storm.phs
    storm = storm.drop(columns=['phs'])
    tags = labels.list_to_latex(storm.columns.to_list(), add_units=True)
    tags[-1] = 'SYM-H\n[nT]'

    # axs = storm.plot(subplots=True, figsize=(8, 15), sharex='none')
    for i, ax in enumerate(axs_col):
        ax.plot(storm.iloc[:, i])
        if s == 0:
            ax.set_ylabel(tags[i], color='black', rotation=0, ha='center',
                va='center')
            ax.get_yaxis().set_label_coords(-0.2, 0.5)
        # if i == len(axs):
        phs = phs.loc[storm_start:storm_end]

        phase.plot_phase_tags(ax, phs, 60, False)

    ax = axs_col[-1]
    y = pred.loc[storm_start:storm_end]
    ax.plot(y.index, y.values, color='r')

    locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
    formatter = mdates.ConciseDateFormatter(locator)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    if s > 0:
        # anchor = 1.1 if s > 0 else 1.05
        ax.legend(['Observed SYM-H', 'Pairwise Network'], bbox_to_anchor=(1, 1.3), loc='upper right')
    else:
        ax.legend(['Observed SYM-H', 'Pairwise Network'], bbox_to_anchor=(1.1, 1.3), loc='upper right')

# for ax in axs[:-1]:
#     ax.set_xticklabels([])

# plt.show()
plt.tight_layout()

plt.subplots_adjust(hspace=0.2)
file_path = os.path.join(env.results_dir, f'pw-eval-storms-comparison.pdf')
plt.savefig(file_path)
# plt.close()

# plt.figure()
# plt.plot(y.index, y.values)
# fig.show()
