import pandas as pd
import matplotlib

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import os

from pairwise_network import env
import symh_dataset

rank_dir = os.path.join(env.results_dir, 'rank')

split_features = True
model_tag = 'p4'

phase_colors = ['#4285f4', '#ea4335', '#fbbc04', ]

# fig.show()
sns.set_style('whitegrid')

file_path = os.path.join(rank_dir, f'pw-rank-{model_tag}.csv')
df = pd.read_csv(file_path)
df['Rank'] *= 100
df['Pair'] = symh_dataset.labels.pair_list_to_latex(df['Pair'], add_star=True)
# df = df[df['Rank'] != 0 ]

def plot_bar(df, ax):
    sns.barplot(x='Pair', y='Rank', hue='Phase', data=df,
        palette=phase_colors, errwidth=2, ax=ax)


if split_features:
    pair_tags = df['Pair'].unique()
    if model_tag == 'p1':
        n_set = len(pair_tags) // 2
        set1 = df[df['Pair'].apply(lambda x: x in pair_tags[:n_set])]
        set2 = df[df['Pair'].apply(lambda x: x in pair_tags[n_set:])]
        fig, axs = plt.subplots(2, figsize=(8,4.5), sharey='all')
        ax = axs[0]
        plot_bar(set1, ax)
        ax.set_xlabel('')
        ax = axs[1]
        plot_bar(set2, ax)
        ax.get_legend().remove()
    else:
        n_set = len(pair_tags) // 3
        set1 = df[df['Pair'].apply(lambda x: x in pair_tags[:n_set])]
        set2 = df[df['Pair'].apply(lambda x: x in pair_tags[n_set:2 * n_set])]
        set3 = df[df['Pair'].apply(lambda x: x in pair_tags[2 * n_set:])]
        fig, axs = plt.subplots(3, figsize=(8,6.5), sharey='all')
        ax = axs[0]
        plot_bar(set1, ax)
        ax.set_xlabel('')
        ax = axs[1]
        plot_bar(set2, ax)
        ax.get_legend().remove()
        ax = axs[2]
        plot_bar(set3, ax)
        ax.get_legend().remove()
    for ax in axs:
        ax.set_ylabel('$\\psi$', rotation=0)
        ax.get_yaxis().set_label_coords(-0.10, 0.5)
else:
    fig, ax = plt.subplots(1, figsize=(8, 4))
    # ax.grid(axis='y')
    plot_bar(df, ax)
# plt.show()
    ax.set_ylabel('$\\psi$', rotation=0)
# fig.show()
fig.tight_layout()
save_file = os.path.join(rank_dir, f'pw-rank-omr-{model_tag}.pdf')
fig.savefig(save_file)
