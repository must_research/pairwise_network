import pandas as pd
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import os

from pairwise_network import env
import symh_dataset

rank_dir = os.path.join(env.results_dir, 'rank')


# if plot_stacked_bar:
#     fig, axs = plt.subplots(3, 1, sharey='col', sharex='all')
#     file_list = ['o.csv', 'm.csv', 'r.csv']
#     seeds = list(range(1, 6))
#
#     for i, phase in enumerate(('Onset', 'Main', 'Recovery')):
#         file_path = os.path.join(rank_dir, file_list[i])
#         df = pd.read_csv(file_path, index_col=0)
#         df /= df.shape[0]
#         ax = axs[i]
#         df.plot.bar(stacked=True, ax=ax, legend=False, title=phase, rot=0)
#         ax.grid(axis='y')
#     axs[-1].legend(seeds, loc='lower center', bbox_to_anchor=(0.5, 0),
#         bbox_transform=fig.transFigure, ncol=5)
#     # fig.legend(seeds, loc='lower center', bbox_to_anchor=(0.5, 0),
#     #     bbox_transform=ax.get_figure().transFigure, ncol=5)
#     # fig.legend(seeds, loc='center', bbox_to_anchor=(1, 0.5))
#     # fig.legend(seeds, loc='lower center', bbox_to_anchor=(0.5, 0), ncol=5)
#     fig.tight_layout()
#
#     fig.show()
#     save_path = os.path.join(env.results_dir, 'rank-omr.png')
#     fig.savefig(save_path)
#
# else:
#     fig, ax = plt.subplots(1)
#     file_path = os.path.join(rank_dir, 'avg.csv')
#     df = pd.read_csv(file_path, index_col=0)
#     df.plot.bar(stacked=False, ax=ax, rot=0, color=['#4285f4', '#ea4335', '#fbbc04',])

model_tag = 'p1'

if model_tag == 'p1':
    split_features = False
else:
    split_features = True

phase_colors = ['#4285f4', '#ea4335', '#fbbc04', ]

# fig.show()
sns.set_style('whitegrid')

file_path = os.path.join(rank_dir, f'{model_tag}-omr.csv')
df = pd.read_csv(file_path)
df['Feature'] = symh_dataset.labels.list_to_latex(df['Feature'])


def plot_bar(df, ax):
    sns.barplot(x='Feature', y='Attribution', hue='Phase', data=df,
        palette=phase_colors, errwidth=2, ax=ax)


if split_features:
    df_imf = df[df['Feature'].str.contains('B')]
    df_plasma = df[df['Feature'].str.contains('B') == 0]
    fig, axs = plt.subplots(2, figsize=(8, 4.5), sharey='all')
    ax = axs[0]
    plot_bar(df_imf, ax)
    ax.set_xlabel('')
    ax = axs[1]
    plot_bar(df_plasma, ax)
    ax.get_legend().remove()
else:
    fig, ax = plt.subplots(1, figsize=(8, 2.5))
    # ax.grid(axis='y')
    sns.barplot(x='Feature', y='Attribution', hue='Phase', data=df,
        palette=phase_colors,
        errwidth=2, ax=ax)
    # sns.stripplot(x='Feature', y='Attribution', hue='Seed', data=df, dodge=False, jitter=0)
    # sns.catplot(x='Phase', y='Attribution', hue='Seed', col='Feature', data=df, dodge=False, jitter=0)
    # sns.catplot(x='Seed', y='Attribution', hue='Phase', col='Feature', data=df)
# plt.show()

fig.tight_layout()
# ax.get_legend().set_bbox_to_anchor((0.33, 1))
# fig.show()
save_file = os.path.join(rank_dir, f'bar-{model_tag}.pdf')
fig.savefig(save_file)



# -----------------------------------------------------------------------------
# OMR Line plot
# -----------------------------------------------------------------------------
# df.Phase = df.Phase.replace({'Onset': 'O', 'Main': 'M', 'Recovery': 'R'})
# tag = df.iloc[0,0]
# tags = df.Feature.unique()
# fig, axs = plt.subplots(1, len(tags), sharey='all')
# for i, tag in enumerate(tags):
#     ax = axs[i]
#     ax.set_title(tag)
#     feature_rank = df[df.Feature == tag]
#     for s in feature_rank.Seed.unique():
#         f = feature_rank[feature_rank.Seed == s]
#         ax.plot(f.Phase, f.Attribution, 'o-')
#     ax.grid(False, axis='x')
#     ax.grid(axis='y')
#
# axs[1].legend([f'Seed {s}' for s in range(1, 6)])
# fig.show()
# -----------------------------------------------------------------------------