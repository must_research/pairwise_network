import pandas as pd
import matplotlib
# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import os

from pairwise_network import env, labels
import symh_dataset


def omr_rank_to_phase(df, phase, pairwise=True):
    if pairwise:
        col = 'Rank'
    else:
        col = 'Attribution'
    result = pd.DataFrame()
    phase_df = df[df.Phase == phase]
    for s in phase_df.Seed.unique():
        result[s] = phase_df[phase_df.Seed == s][col]
    result['Avg'] = result.mean(axis=1)
    return result


rank_dir = os.path.join(env.results_dir, 'rank')

# model = 'p1'
# model = 'p2'
# model = 'p3'
model = 'p4'

omr = True
# omr = False

pairwise_rank = True
# pairwise_rank = False

if omr:
    if pairwise_rank:
        gridspec_kw = None
        if model == 'p1':
            figsize = (10, 5)
            cbar_aspect = 30
        elif model == 'p2':
            figsize = (10, 10)
            cbar_aspect = 70
        elif model == 'p3':
            figsize = (10, 10)
            cbar_aspect = 70
        elif model == 'p4':
            figsize = (10, 10)
            cbar_aspect = 70
    else:
        if model in ['p1', 'p3']:
            figsize = (8, 3)
            gridspec_kw = None
            cbar_aspect = 20
        else:
            figsize = (10, 5)
            cbar_aspect = 30
            gridspec_kw = {'wspace': -0.1}

    if pairwise_rank:
        file_path = os.path.join(rank_dir, f'pw-omr-{model}.csv')
        df = pd.read_csv(file_path)
        df.Pair = symh_dataset.labels.pair_list_to_latex(df.Pair, add_star=True)
        df = df.set_index('Pair')
        df['Rank'] *= 100
        cmap = sns.light_palette("seagreen", as_cmap=True)
    else:
        file_path = os.path.join(rank_dir, model + '-omr.csv')
        df = pd.read_csv(file_path)
        df.Feature = symh_dataset.labels.list_to_latex(df.Feature)
        df = df.set_index('Feature')
        cmap = sns.color_palette("flare", as_cmap=True)
        cmap = cmap.reversed()


    fig, axs = plt.subplots(1, 3, figsize=figsize, gridspec_kw=gridspec_kw)

    sns.set_style('whitegrid')
    sns.set()

    df_list = list()
    for i, p in enumerate(df.Phase.unique()):
        df_phase = omr_rank_to_phase(df, p)

        df_phase = df_phase.sort_values(by='Avg', ascending=False)

        ax = axs[i]
        col = 'Rank'
        # col = 'Attribution'
        sns.heatmap(df_phase, cmap=cmap, square=True, linewidths=.5, ax=ax,
            annot=True, fmt='.0f', vmin=0, vmax=df[col].max(),
            cbar=p == 'Recovery', cbar_kws={'aspect': cbar_aspect})
        ax.tick_params(axis='x', rotation=0)
        ax.tick_params(axis='y', rotation=0)
        ax.set_ylabel('')
        ax.set_xlabel('Seed')
        ax.set_title(p)
else:
    if model in ['p1', 'p3']:
        figsize = (3.5, 3.5)
        cbar_aspect = 20
    else:
        figsize = (4, 6)
        cbar_aspect = 30

    file_path = os.path.join(rank_dir, model + '.csv')
    df = pd.read_csv(file_path)
    df['Feature'] = symh_dataset.labels.list_to_latex(df['Feature'])
    df = df.set_index('Feature')
    df = df.sort_values(by='Avg', ascending=False)
    # df = df.transpose()

    # sns.set_style('whitegrid')
    # sns.set()
    cmap = sns.color_palette("flare", as_cmap=True)
    cmap = cmap.reversed()
    # cmap = sns.diverging_palette(220, 10, as_cmap=True)
    fig, ax = plt.subplots(figsize=figsize)
    sns.heatmap(df, cmap=cmap, square=True, linewidths=.5, ax=ax, annot=True,
        fmt='.0f', cbar_kws={'aspect': cbar_aspect})
    ax.tick_params(axis='x', rotation=0)
    ax.tick_params(axis='y', rotation=0)
    ax.set_ylabel('')
    ax.set_xlabel('Seed')

fig.tight_layout()

fig.show()
save_file = os.path.join(rank_dir, f'pw-omr-{model}.pdf')
fig.savefig(save_file)
