import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import re
import os

from wandb import save

from pairwise_network import wandb_api, env, labels, dataset
import symh_dataset

env.set_log_level()


def id_list_to_sn_act_dist(run_id_list, partition='train', phase=None,
        add_star=True):
    df_dist = pd.DataFrame()
    df_tags = pd.DataFrame()
    for run_id in run_id_list:
        run = wandb_api.run_id_to_run(run_id)
        if phase is not None:
            phase_mask = generate_phase_mask(run, partition, phase)
        else:
            phase_mask = None
        dist, tags = wandb_api.run_to_sn_act_dist(run, partition=partition,
            rename_subnets=True, weighted=True, add_star=add_star,
            return_renamed_tags=True, act_mask=phase_mask)
        col_name = run.config['seed']
        # col_name = run_id
        df_dist[col_name] = dist
        df_tags[col_name] = pd.Series(tags)
    return df_dist, df_tags


def calc_feature_ranking(id_list, partition='train', phase=None):
    df_dist, df_tags = id_list_to_sn_act_dist(id_list, partition, phase,
        add_star=False)
    result = pd.DataFrame()
    for seed in df_dist.columns:
        rank = dict()
        for i, (old_tag, ratio) in enumerate(df_dist[seed].iteritems()):
            if i < df_tags.shape[0]:
                renamed_tag = df_tags[seed][i]
            else:
                renamed_tag = old_tag
            features = labels.split_pairs(renamed_tag)
            features_phs_removed = list()
            for x in features:
                if 'phs' not in x:
                    features_phs_removed.append(x)
            for x in features_phs_removed:
                if 'phs' not in x:
                    if x not in rank.keys():
                        rank[x] = 0
                    rank[x] += ratio / len(features_phs_removed)
        result[seed] = pd.Series(rank)
    result = result.fillna(0)

    result = result.sort_values(by=[1, ], ascending=False)
    result *= 100

    result = result.loc[result.index != 'bias']
    result.index = replace_index(result.index)

    return result


def generate_phase_mask(run, partition, phase):
    """Create a boolean array corresponding to the phase.

    Args:
        partition (str):
        phase (int): 1/2/3

    """
    cfg = wandb_api.run_to_cfg(run)
    env.set_seeds(cfg['seed'])
    # Dataset
    df_datasets = symh_dataset.create_df_partitions('symhv3', env.data_dir,
        include_phase=True, encode_phase=False,
        timeshift=cfg['timeshift'], interpol_limit=10, standardize=False,
        drop_missing=True)
    df = dataset.datasets_to_partition(df_datasets, partition)
    return np.array(df.phs == phase)


def replace_index(index):
    index = labels.tags_replace_substring(index, '_m', '')
    index = labels.tags_replace_substring(index, 'phs3', 'p3')
    index = labels.tags_replace_substring(index, 'phs2', 'p2')
    index = labels.tags_replace_substring(index, 'phs', 'p1')
    return index


# no-interpol
# p1 = ['8fjffld1', 'tp9nm9q4', 'h66xkss6']
# p2 = ['dcgsu375', 'm76a25w7', 'hqy0850i']
# p3 = ['nwekhjva', '2u9u20yb', 'jwztl3bu']
# p4 = ['5r422bwc', 'qzctlm45', 'jkpgh20l']

p1 = ['paqu81ki', '0y9s4b6u', 'eja69o04', '1k1cq6j3', '3hkky5gz']
p2 = ['m43xus3z', 'yidbehpb', '7z5wsq5h', '3e18ngxr', '1n6526sr']
p3 = ['rmeytqwz', 'fnxesyxm', 'bh6236td', '2hlkguko', 'jhg1odg7']
p4 = ['10pgr83m', '6vxwlkkw', 'hpo3g3nh', 'k0jhnbi5', '2ytmlwj7']

model_tag = 'p2'

if model_tag == 'p1':
    id_list = p1
    figsize = (3.5, 5)
    gridspec_kw = None
    cbar_aspect = 20
elif model_tag == 'p2':
    id_list = p2
    figsize = (3.5, 9)
    cbar_aspect = 40
elif model_tag == 'p3':
    id_list = p3
    figsize = (3.5, 9)
    cbar_aspect = 40
elif model_tag == 'p4':
    id_list = p4
    figsize = (3.5, 9)
    cbar_aspect = 40

partition = 'eval'

rank_dir = os.path.join(env.results_dir, 'rank')

separate_phases = True
# separate_phases = False

calc_dist = True
# calc_dist = False

# plot_pw_heatmap = True
plot_pw_heatmap = False

# calc_feature_rank = True
calc_feature_rank = False

# calc_phase_bar = True
calc_phase_bar = False

# pairwise_rank = True
pairwise_rank = False
# -----------------------------------------------------------------------------
# Calculate wsnr-distribution
# -----------------------------------------------------------------------------
if calc_dist:
    if separate_phases:
        rank_list = list()
        for p in (1, 2, 3):
            df, _ = id_list_to_sn_act_dist(id_list, partition, p)
            rank_list.append(df)
        df_dist = pd.concat(rank_list, axis=1)
        df_dist = df_dist.sort_index(1)
    else:
        df_dist, df_tags = id_list_to_sn_act_dist(id_list, partition, None)
        for c in df_tags.columns:
            df_tags[c] = replace_index(df_tags.loc[:, c])
        df_tags.to_clipboard()

    df_dist *= 100
    df_dist.index = replace_index(df_dist.index)
    df_dist.to_clipboard()
    print(df_dist.to_markdown())

    save_file = os.path.join(rank_dir, f'pw-omr-heat-{model_tag}.csv')
    df_dist.to_csv(save_file)

    if plot_pw_heatmap:
        df = df_dist.loc[(df_dist != 0).any(1)]

        df['Avg'] = df.mean(axis=1)
        df = df.sort_values(by='Avg', ascending=False)

        symh_dataset.labels.df_index_to_latex(df)

        # cmap = sns.color_palette("flare", as_cmap=True)
        # cmap = sns.color_palette("viridis", as_cmap=True)
        cmap = sns.light_palette("seagreen", as_cmap=True)
        # cmap = cmap.reversed()
        fig, ax = plt.subplots(figsize=figsize)
        sns.heatmap(df, cmap=cmap, square=True, linewidths=.5, ax=ax,
            annot=True, fmt='.0f', cbar_kws={'aspect': cbar_aspect})
        ax.tick_params(axis='x', rotation=0)
        ax.tick_params(axis='y', rotation=0)
        ax.set_ylabel('')
        ax.set_xlabel('Seed')

        fig.tight_layout()

        fig.show()
        save_file = os.path.join(rank_dir, f'pw-rank-{model_tag}.pdf')
        fig.savefig(save_file)


# -----------------------------------------------------------------------------
# Calculate distributed feature ranking
# -----------------------------------------------------------------------------
# Divide the wsnr of each subnet by the number of inputs (assuming each input
# contribute equally) and assign to each feature.

if calc_feature_rank:
    if separate_phases:
        rank_list = list()
        for p in (1, 2, 3):
            df = calc_feature_ranking(id_list, partition, p)
            rank_list.append(df)
        df_rank = pd.concat(rank_list, axis=1)
        df_rank = df_rank.sort_index(1)
    else:
        df_rank = calc_feature_ranking(id_list, partition, None)

    df_rank.to_clipboard()
    print(df_rank.to_markdown())


# -----------------------------------------------------------------------------
# Draw phase bar chart
# -----------------------------------------------------------------------------

# Horizontal
# if calc_phase_bar:
#     rank_list = list()
#     for i, phase in enumerate(('Onset', 'Main', 'Recovery')):
#         if pairwise_rank:
#             df, df_tags = id_list_to_sn_act_dist(id_list, partition, i + 1,
#                 add_star=False)
#             rank_list.append(df)
#         else:
#             df = calc_feature_ranking(id_list, partition, i + 1)
#             rank_list.append(df)
#     df_rank = pd.concat(rank_list, axis=1)
#     df_rank = df_rank.sort_index(1)
#     df_rank.to_clipboard()

# Vertical
if calc_phase_bar:
    # result = pd.DataFrame()
    rank_list = list()
    for p, phase in enumerate(('Onset', 'Main', 'Recovery')):
        df, _ = id_list_to_sn_act_dist(id_list, partition, p + 1,
            add_star=False)
        df = df.loc[(df != 0).any(1)]
        df.index.rename('Pair', inplace=True)
        df = df.reset_index()
        df = df.melt(id_vars='Pair', var_name='Seed', value_name='Rank')
        df['Phase'] = pd.Series(np.full(len(df), phase))
        rank_list.append(df)
    df_rank = pd.concat(rank_list, axis=0)
    # df_rank.to_clipboard()

    save_file = os.path.join(rank_dir, f'pw-rank-{model_tag}.csv')
    df_rank.to_csv(save_file)
    # fig, ax = plt.subplots(1, figsize=(8, 4))
    # # ax.grid(axis='y')
    # sns.barplot(x='Feature', y='Attribution', hue='Phase', data=df_rank,
    #     palette=['#4285f4', '#ea4335', '#fbbc04', ],
    #     errwidth=2, ax=ax)
    # fig.show()