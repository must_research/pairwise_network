from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="pairwise_network",
    version="0.0.1",
    author="Jacques Beukes",
    author_email="jpbeukes27@gmail.com",
    description="Package for training and testing pairwise networks.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="pairwise neural network deep learning",
    url="https://JPBeukes@bitbucket.org/must_research/pairwise_network.git",
    packages=find_packages(),
    python_requires='>=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",  # TODO: Add License
        "Operating System :: OS Independent"
    ],
    # install_requires=[  # TODO: Check minimum versions
    #     'pytorch>=1.4',  # TODO: add cuda as optional requirement
    #     'pandas>=1.0.0',
    #     'scikit-learn',
    #     'sympy',
    #     'numpy',
    #     'pyyaml',
    #     'matplotlib',
    # ],
    extras_require={
        'interactive': ['wandb']
    },
    entry_points = {
        'console_scripts': [
            'pw-build=pairwise_network.build:main',
        ]
    }
)
