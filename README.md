# Pairwise Network

PyTorch based package for creating, training and analysing pairwise networks, as introduced in ["Input parameter ranking for neural networks in
a space weather regression problem" (Lotz et al. 2019)](http://ceur-ws.org/Vol-2540/FAIR2019_paper_50.pdf).

## Architecture
The figure below show the architecture of a pairwise network, when used for a simple regression problem where three input parameters (x1, x2, x3) are used to predict a single target parameter (y).   

![https://bitbucket.org/must_research/pairwise_network/src/master/pairwise-architecture-vinilla.png](pairwise-architecture-vinilla.png)

## Implementation

## Installation

To install the package:
```shell script
pip install -e .
```

To use the package for training models:
```shell script
pip install -e .[interactive]
```
This will install additional packages required for training, testing and
 analysing the pairwise network. 

## API

### Model

### Train, Test

### Analyse

### Datasets

**SYM-H**

**Artificial Datasets**
 